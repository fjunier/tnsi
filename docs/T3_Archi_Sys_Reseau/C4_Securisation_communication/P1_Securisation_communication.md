---
title:  Sécurisation des communications
---

#  Sécurisation des communications

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}


* [Cours avec exercices](https://nuage03.apps.education.fr/index.php/s/BkJWeDjCxAoEgrS)
* [Correction du cours avec exercices](https://nuage03.apps.education.fr/index.php/s/BkJWeDjCxAoEgrS)
* [Synthèse de cours](ressources/TNSI-Synthese-SecurisationCommunication-2022.pdf)
* [💻 Chiffrements de César et Vigenère sur Capytale](https://capytale2.ac-paris.fr/web/c/3ed5-1533001){: .md-button}
