---
title:  Processus
---

#  Processus

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}


* [Cours avec exercices](ressources/TNSI-Cours-Processus-2023.pdf)
* [Correction du cours avec exercices](ressources/Correction-TNSI-Cours-Processus-2023.pdf)
* [Scripts processus](ressources/scripts_processus.zip)
* [Synthèse de cours](ressources/TNSI-Synthese-Processus-2022.pdf)
* [💻 TP ordonnancement sur Capytale](https://capytale2.ac-paris.fr/web/c/add5-626257){: .md-button}