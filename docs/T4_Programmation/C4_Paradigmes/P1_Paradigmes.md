---
title:  Paradigmes
---

#  Paradigmes

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}

* [💻 TP/Cours   sur Capytale](https://capytale2.ac-paris.fr/web/c/7aa8-1369486){: .md-button}
* [Synthèse du cours Paradigmes de programmation](https://digipad.app/p/767233/73c8cf8f5ab04)