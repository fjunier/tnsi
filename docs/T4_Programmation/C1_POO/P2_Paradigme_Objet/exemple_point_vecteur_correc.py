import math
import doctest

class Point1:
    """Classe de fabrication d'un point du plan"""
    
    def __init__(self, x, y):
        """Constructeur d'un point à partir de ses coordonnées
        >>> p1 = Point1(0, 0)
        """
        self.x = x
        self.y = y
        
    def distance(self, autre):
        """Méthode qui renvoie la distance d'un point à un autre point"""
        return math.sqrt((self.x - autre.x) ** 2 + (self.y - autre.y) ** 2)
    
    def translation(self, vect):
        """Méthode qui renvoie le point obtenu à partir de self
        par translation de vecteur vect"""
        # à compléter
        vx = vect.x
        vy = vect.y
        return Point1(self.x + vx, self.y + vy)
    
    def égalité(self, autre):
        """Méthode renvoyant un booléen déterminant 
        si les points self et autre sont égaux
        >>> p1 = Point1(3, 4)
        >>> v1 = Vecteur(-2, 5)
        >>> p2 = p1.translation(v1)
        >>> p3 = Point1(1, 9)
        >>> p2.égalité(p3)
        True
        """
        # à compléter
        return self.x == autre.x and self.y == autre.y
    
class Vecteur:
    """Classe de fabrication d'un vecteur du plan"""
    
    def __init__(self, x, y):
        """
        Constructeur d'un vecteur à partir de ses coordonnées
        >>> v1 = Vecteur(-2, 5)
        """
        self.x  = x  # attribut x
        self.y = y   # attribut y 
    
    def addition(self, autre):
        """Méthode d'addition à un autre vecteur"""
        xa = autre.x
        ya = autre.y
        return Vecteur(self.x + autre.x, self.y + autre.y)
    
    def égalité(self, autre):
        """Méthode renvoyant un booléen déterminant 
        si les vecteurs self et autre sont égaux
        """
        # à compléter
        return self.x == autre.x and self.y == autre.y
    
    
    def mult_scal(self, k):
        """
        Méthode qui renvoie un nouveau vecteur k * self k scalaire
        >>> v1 = Vecteur(-2, 5)
        >>> v2 = v1.mult_scal(3)
        >>> v3 = Vecteur(-6, 15)
        >>> v2.égalité(v3)
        True
        """
        # à compléter
        return Vecteur(self.x * k, self.y * k)
    
# On exécute les tests inclus dans les docstrings
# à décommenter
#doctest.testmod(verbose=True)