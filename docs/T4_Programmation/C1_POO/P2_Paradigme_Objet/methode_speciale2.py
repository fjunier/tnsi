class Point5:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def __eq__(self, autre):
        return self.x == autre.x  and self.y == autre.y
    
    def __str__(self):
        # on utilise une chaine formattée f-string 
        # # voir https://docs.python.org/3/reference/lexical_analysis.html#f-strings
        return f"({self.x}, {self.y})"
    
    def __repr__(self):
        return f"Point5({self.x}, {self.y})"
    
        
        

p1 = Point5(10, 3)
p2 = Point5(10, 3)
print("p1 == p2 : ", p1 == p2)
print(p1)  # test de la méthode d'affichage lisible __str__
representation_jolie = str(p2)
representation_syntaxique = repr(p2)
# affichez les deux représentations  dans la console
# pour tester  la méthode d'affichage syntaxique __repr__
# évaluez p1 ou p2 dans la console
