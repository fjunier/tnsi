class Point2:
        
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
p1 = Point2(1, 2)
p2 = Point2(1, 2)
p3 = p2
print("p1 == p2 : ", p1 == p2)
print("Identifiant de p1 : ", id(p1))
print("Identifiant de p2 : ", id(p2))
print("p1 is p2 ", p1 is p2)
print("p3 == p2 : ", p3 == p2)
print("Identifiant de p3 : ", id(p3))
print("p3 is p2 ", p3 is p2)
        