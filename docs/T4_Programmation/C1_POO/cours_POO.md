---
title: Cours POO  🎯
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.

Parties du cours :

* [Partie 1 Structurer avec des classes](./P1_Stucturer_avec_des_classes/P1_cours_POO.md)
* [Partie 2  Paradigme objet](./P1_Stucturer_avec_des_classes/P1_cours_POO.md)