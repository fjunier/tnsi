---
title:  Modularité et mise au point
---

#  Modularité et mise au point

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}


* [Cours avec exercices](ressources/TNSI-Cours-Module-Test-2022.pdf)
* [Matériel module/test](https://nuage03.apps.education.fr/index.php/s/o7tWBz7EPHdjGsq)