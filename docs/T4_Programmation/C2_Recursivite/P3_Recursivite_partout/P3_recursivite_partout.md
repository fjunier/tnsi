---
title:  La récursivité partout
---

# La récursivité partout

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [cours d'Olivier Lécluse](https://www.lecluse.fr/nsi/NSI_T/langages/recursivite/)
    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [manuel NSI](https://www.mesmanuels.fr/acces-libre/9782017189992) chez Hachette sous la direction de *Michel Beaudouin Lafon*
    * la [ressource Eduscol sur la récursivité](https://eduscol.education.fr/document/10103/download)
    * le [cour e-nsi sur la récursivité](https://e-nsi.forge.aeif.fr/recursif)
    * le cours de mon collègue Pierre Duclosson


 [🔖 Synthèse de ce qu'il faut retenir pour le bac](../Synthese/synthese_recursivite.pdf){: .md-button}



# Objet récursif

!!! note "Point de cours 2"
    Lorsque la définition d'un objet fait appel à l'objet lui même, on parle de définition **récursive.**

!!! example "Exemple 2"
    On trouve beaucoup de définitions récursives en informatique :

    * pour définir une structure de données récursive :
        * un *arbre binaire* est soit vide, soit un noeud qui a pour fils deux arbres binaires
        * une *liste* est soit vide, soit un couple constitué d'une valeur et d'une référence vers une liste
    * dans des *acronymes*, pratique héritée des communautés de *hackers* des campus des années 1970 :
        * le sigle du projet de système d'exploitation libre [GNU](https://en.wikipedia.org/wiki/GNU) est un acronyme négatif : GNU'S NOT UNIX
        * le sigle du langage de serveur Web [PHP: Hypertext Preprocessor](https://en.wikipedia.org/wiki/PHP) contient sa définition : PHP: Hypertext Preprocessor 


# Récursivité en géométrie et dans l'art

??? example "Exemple 3 : figures fractales"
    Une figure [fractale](https://fr.wikipedia.org/wiki/Fractale) présente une structure similaire à toutes les échelles.

    On peut en donner une définition récursive : un élément d'une figure
    fractale est une figure fractale.

    Le terme **fractale**, qui vient du latin *fractus* (cassé), est apparu
    pour la première fois en 1975, dans le livre *Les objets fractals :
    forme, objet et dimension*, de [Benoît
    Mandelbrot](https://fr.wikipedia.org/wiki/Beno%C3%AEt_Mandelbrot),
    ancien élève du lycée du Parc.

    Les premières courbes fractales sont apparues au début du XX siècle avec
    par exemple [la courbe de Von  Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch).

    On peut la créer à partir d'un segment de droite, en modifiant
    récursivement chaque segment de droite (*cas de base*) de la façon
    suivante (*réduction*) :

    -   **Étape 1 :** On divise le segment de droite en trois
        segments de longueurs égales.

    -   **Étape 2 :** On construit un triangle équilatéral
        ayant pour base le segment médian de la première étape.

    -   **Étape 3 :** On supprime le segment médian qui était
        la base du triangle de la deuxième étape.

    *Source : article de Wikipedia.*

    Une structure fractale étant infinie, on ne peut représenter qu'une
    courbe approchée d'une courbe fractale par itérations successives.

    En pratique, il est plus simple de procéder récursivement à partir de la
    dernière itération pour construire la courbe approchée par réductions
    successives jusqu'au cas de base.

    On peut mettre en évidence le *cas de base* et la *réduction* pour la
    courbe de Koch à partir des trois premières itérations.

    **Cas de base: itération 1** ![image](images/koch1.png)

    **Itération 2** ![image](images/koch2.png)

    **Itération 3 avec réduction** ![image](images/koch3.png)

    ??? info "Flocon de Von Koch"
        ![flocon](images/flocon_koch.png)

!!! example "Exemple 4"
    Les figures récursives qui se contiennent elles-mêmes sont fréquentes dans la publicité ou chez certains artistes comme [M.C. Escher](https://fr.wikipedia.org/wiki/Maurits_Cornelis_Escher) qui s'est intéressé à la représentation de l'infini.

    ??? bug "Publicité de la Vache Qui Rit"

        ![Vache](images/vache.gif)

        > Source : Création de l'artiste visuel Polonais Feliks Tomasz Konczakowski 

        
    ??? bug "Tableau de M.C. Escher"

        Dans une galerie d'art, un jeune homme regarde une estampe. En fait  le jeune homme est dans le tableau qu'il est en train de regarder. C'est une *mise en abyme*. 

        ![escher](images/escher.jpg)

# Récursivité dans le langage et la  littérature


!!! example "Exemple 5"

    Citons [l'article de Wikipédia sur la récursivité](https://fr.wikipedia.org/wiki/R%C3%A9cursivit%C3%A9) :

    > *[...] une phrase peut être définie récursivement (à peu près) comme quelque chose qui possède une structure qui inclut une phrase nominale, un verbe et une autre phrase (optionnelle). C'est vraiment un cas spécial dans lequel la définition mathématique de la récursivité se manifeste. [...]*

    En littérature, la récursivité peut s'exprimer à travers le procédé de [mise en abyme](https://fr.wikipedia.org/wiki/Mise_en_abyme) où une oeuvre est représentée dans une oeuvre similaire. Par exemple dans son roman *Les Faux Monnayeurs*, [André Gide](https://fr.wikipedia.org/wiki/Andr%C3%A9_Gide) introduit un personnage en train d'écrire un roman intitulé *Les Faux Monnayeurs*. 
    
    Ce procédé se retrouve également au cinéma  ([Last Action Hero](https://fr.wikipedia.org/wiki/Last_Action_Hero)) ou au théâtre ([L'illusion comique](https://fr.wikipedia.org/wiki/L%27Illusion_comique)).
