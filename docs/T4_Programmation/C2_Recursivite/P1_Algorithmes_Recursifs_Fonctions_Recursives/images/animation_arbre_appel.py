
from PIL import Image
anim = []
for k in range(1, 6):
    anim.append(Image.open(f"arbre_appel_pgcd{k}.png")) 
    
anim[0].save("animation_arbre_appel.gif",
                save_all=True, append_images=anim[1:], optimize=True, duration=50, loop=5)
