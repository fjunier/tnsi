---
title:  Correction et terminaison  🎯
---

# Correction et terminaison (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [cours d'Olivier Lécluse](https://www.lecluse.fr/nsi/NSI_T/langages/recursivite/)
    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [manuel NSI](https://www.mesmanuels.fr/acces-libre/9782017189992) chez Hachette sous la direction de *Michel Beaudouin Lafon*
    * la [ressource Eduscol sur la récursivité](https://eduscol.education.fr/document/10103/download)
    * le [cour e-nsi sur la récursivité](https://e-nsi.forge.aeif.fr/recursif)
    * le cours de mon collègue Pierre Duclosson


 [🔖 Synthèse de ce qu'il faut retenir pour le bac](../Synthese/synthese_recursivite.pdf){: .md-button}

## Étude de cas


!!! question "Exercice 3"

    🎯 _Analyser le fonctionnement d'un programme récursif_

    === "Énoncé"
        1. On veut compléter la fonction récursive `puissance_rec` dans l'IDE ci-dessous pour que `puissance_rec(x, n)` renvoie la valeur $x^{n}$ sans utiliser l'opérateur d'exponentiation `**` de Python.
   
             a. Déterminer un **cas de base**.

             b. Comment peut-on déterminer `puissance_rec(x, n)` à partir de `puissance_rec(x, n - 1)` (**réduction**) ?

             c. Compléter le code.
   
        {{IDE("exercice3_puissance")}}

        2. On va démontrer la *correction* de la  fonction `puissance_rec`, c'est-à-dire que la propriété $P(n): puissance\_rec(x, n) = x^{n}$ est vraie pour tout  entier naturel $n$ positif. 
        
            !!! tip "Méthode 2 : preuve de correction par récurrence"
                La correction d'une fonction récursive se démontre à l'aide d'un *raisonnement* par récurrence comme en Mathématiques :

                * *Initialisation* :  on démontre que la propriété est vraie pour le ou les cas de base (sans en oublier), ici pour $n=0$.
                * *Hérédité* : on démontre que pour tout entier naturel $n$, si on suppose que $P(n)$ est vraie alors $P(n+1)$ est encore vraie.

            Appliquez le raisonnement par récurrence pour démontrer que $P(n)$ est vraie pour tout entier naturel $n$ et donc que la fonction `puissance_rec` est correcte.

        3. On a vu dans l'exercice 1 que comme pour un algorithme *itératif* avec une boucle non bornée, il est possible qu'un algorithme *récursif* ne se termine pas.
            
            Déplier et lire le paragraphe suivant puis justifier que `puissance_rec` se termine.

            ??? warning "Récursivité et terminaison"
                Une fonction récursive ne se termine pas si les étapes de réduction ne font pas converger les appels récursifs vers un cas de base.

                Dans l'exercice 1, on a vu que celà peut se produire :

                * Si la fonction ne propose pas de cas de base :

                ~~~python
                def factorielle(n):
                    """Fausse"""
                    # réduction mais pas de cas de base
                    return n * factorielle(n-1)
                ~~~
                
                * Si la fonction implémentée est appelée sur une valeur en dehors du domaine de définition de l'algorithme récursif :

                ~~~python
                def factorielle(n):
                    """Renvoie n! = 1 *2 * ... (n-1) * n pour tout entier n >= 0"""
                    # cas de base
                    if n == 0:
                        return 1
                    # réduction 
                    return n * factorielle(n-1)
                
                # Appel sur un nombre négatif => descente infinie
                factorielle(-1)
                ~~~

                On peut prévenir ces problèmes en vérifiant des préconditions (**programmation défensive**).

                ~~~python
                def factorielle(n):
                    """Renvoie n! = 1 *2 * ... (n-1) * n pour tout entier n >= 0"""
                    assert isinstance(n, int) and n >= 0
                    # cas de base
                    if n == 0:
                        return 1
                    # réduction 
                    return n * factorielle(n-1)
                
                # Appel sur un nombre négatif => descente infinie
                factorielle(-1)
                ~~~


                * Si dans la fonction implémentée, la réduction ne fait pas converger les appels récursifs vers un cas de base. Par exemple dans le cas ci-dessous  l'appel `factorielle(1)` ne se termine pas car `factorielle(1)` appelle  `factorielle(-1)` qui appelle `factorielle(-3)` etc ... Donc le cas de base 0 a été sauté et ne sera jamais atteint.

                ~~~python
                def factorielle(n):
                    """Fausse"""
                    # cas de base
                    if n == 0:
                        return 1
                    # réduction fausse
                    return n * factorielle(n - 2)
                ~~~
                
                > 💡 _Comme pour une boucle, on peut démontrer que fonction récursive se termine, à l'aide d'un **variant**, une quantité entière positive dont on démontre qu'elle décroît lors de chaque appel récursif jusqu'à ce que le cas de base soit atteint._
            
    === "Correction"
        Au tableau ...
