# --- HDR ---#
DICO_TRACE = dict()


def trace(f):
    """Remplace une fonction f par une fonction enrobante g."""

    def g(*args):
        """Fonction enrobante (wraped en anglais)."""
        arguments = f"{args[0]}"

        if g.trace_pile:
            g.pile.append(f"Contexte de {g.__name__}{tuple(e for e in args)}")
            lmax = max([len(contexte) for contexte in g.pile])
            print(f"Appel de {g.__name__}({arguments}) => descente \n")
            print("État de la pile : \n")
            for contexte in g.pile[::-1]:
                print("|{0:^{1}}|".format(contexte, lmax))
                print(f"|{lmax * '-'}|")
            print("\n" * 3)
        if not (g.trace_pile) and g.trace:
            print(
                "| " * (g.indentation - 1)
                + "┌"
                + f"Appel de {g.__name__}({arguments}) => descente"
            )
            g.indentation += 1
            g.__dict__["indentation"] = g.indentation
        retour = f(*args)
        if not (g.trace_pile) and g.trace:
            g.indentation -= 1
            g.__dict__["indentation"] = g.indentation
            print(
                "| " * (g.indentation - 1)
                + "└"
                + f"Fin de {g.__name__}({arguments}) <= remontée"
            )
        if g.trace_pile:
            print(f"Fin de {g.__name__}({arguments}) <=  remontée \n")
            lmax = max([len(contexte) for contexte in g.pile])
            print("État de la pile : \n")
            for contexte in g.pile[::-1]:
                print("|{0:^{1}}|".format(contexte, lmax))
                print(f"|{lmax * '-'}|")
            print("\n" * 2)
            g.pile.pop()
        return retour

    # on vérifie si le nom de f apparaît déjà dans  table des fonctions tracées
    if f.__name__ not in DICO_TRACE:
        g.__name__ = f.__name__  # g portera le même que f
        g.indentation = 1  # compteur d'indentation attaché à g
        g.trace = True  # booléen pour activer ou désactiver la trace
        g.trace_pile = False
        g.pile = []
        DICO_TRACE[f.__name__] = g
    return DICO_TRACE[f.__name__]


def desactiver_trace(fonction):
    if fonction.__name__ in DICO_TRACE:
        del DICO_TRACE[fonction.__name__]
        fonction.trace = False


def activer_trace_pile(fonction):
    if fonction.__name__ in DICO_TRACE:
        fonction.trace_pile = True


def desactiver_trace_pile(fonction):
    if fonction.__name__ in DICO_TRACE:
        fonction.trace_pile = False


# --- HDR ---#

def puissance_rec(x, n):
    """
    Renvoie x ** n sans utiliser l'opérateur **
    Fonction récursive    

    Paramètres:
        x (int): entier x
        n (int): exposant >= 0
    """
    # à compléter
    
def test_puissance_rec(x, n):
    assert puissance_rec(2, 0) == 1, "échec sur puissance_rec(2, 0)"
    assert puissance_rec(2, 1) == 2, "échec sur puissance_rec(2, 1)"
    assert puissance_rec(2, 2) == 4, "échec sur puissance_rec(2, 2)"
    assert puissance_rec(2, 3) == 8, "échec sur puissance_rec(2, 3)"
    assert puissance_rec(3, 2) == 9, "échec sur puissance_rec(3, 2)"
    assert puissance_rec(17, 9) == 118587876497, "échec sur puissance_rec(17, 9) == 118587876497"