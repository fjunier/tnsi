def murs_enveloppe(n):
    """
    Fonction enveloppe qui vérifie une précondition sur n
    On évite ainsi de vérifier la précondition pourchaque appel récursif
    """
    assert isinstance(n, int) and n >= 0, "la longeur n doit être un entier positif"
    return murs(n)

def murs(n):
    """Fonction récursive"""
    # cas de base à compléter
    # réduction et appels récursifs  
