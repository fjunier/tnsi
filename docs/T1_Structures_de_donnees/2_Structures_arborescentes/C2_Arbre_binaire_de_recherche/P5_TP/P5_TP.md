---
title: TP 🎯
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


# TP Arbres binaires de recherche (Bac 🎯)

*  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
*  [💻 TP 2 _File de priorité_ sur Capytale](https://capytale2.ac-paris.fr/web/c/31c0-2974096)
  