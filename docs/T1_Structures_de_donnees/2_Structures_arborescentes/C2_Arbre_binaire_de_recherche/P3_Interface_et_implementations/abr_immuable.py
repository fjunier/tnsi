class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)
    
# Interface fonctionnelle minimale pour Arbre Binaire de Recherche

def abr_vide():
    """Renvoie un arbre binaire de recherche vide représenté par None"""
    return None

def est_vide(abr):
    """Teste si un arbre binaire de recherche est vide, renvoie un booléen"""
    return abr is None

def gauche(abr):
    """Renvoie le sous-arbre fils gauche de  l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.gauche

def droit(abr):
    """Renvoie le sous-arbre fils droit de  l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.droit

def element_racine(abr):
    """Renvoie l'élément à la racine de l'arbre binaire de recherche abr
    Provoque une erreur si arbre est vide"""
    assert not est_vide(abr), "Arbre vide"
    return abr.element

# Extension de l'interface 
def afficher_arbre(abr):
    """Affichage syntaxiquement correct d'un arbre binaire de recherche abr construit avec la classe Noeud
    Analogue à la fonction builtin repr"""
    if est_vide(abr):
        return repr(None)
    return f"Noeud({afficher_arbre(abr.gauche)}, {repr(abr.element)}, {afficher_arbre(abr.droit)})"

def test_abr_immuable():
    """Test unitaires pour l'exercice 3"""
    a = Noeud(Noeud(None, 4, Noeud(None, 6, None)), 8, Noeud(Noeud(None, 9, None), 10, Noeud(None, 12, None)))
    assert taille(a) == 6
    assert hauteur(a) == 3
    assert maximum(a) == 12
    assert minimum(a) == 4
    assert propriete_abr(a) == True
    tab = []
    parcours_infixe(a, tab)
    assert tab == [4, 6, 8, 9, 10, 12]
    b = Noeud(Noeud(None, 4, Noeud(None, 3, None)), 8, Noeud(Noeud(None, 9, None), 10, Noeud(None, 12, None)))
    assert propriete_abr(b) == False
    print("Tests réussis")