class Noeud4:
    """classe de noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)


class ABR2:
    """Classe d'arbre binaire mutable"""
    
    def __init__(self):
        """Constructeur, self.racine point vers None si arbre vide
        ou le noeud racine"""
        self.racine = None

    def est_vide(self):
        """Teste si l'arbre est vide, renvoie un booléen"""
        return self.racine is None

    def droit(self):
        """Renvoie le sous-arbre (de type Arbre) fils droit de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.droit
    
    def gauche(self):
        """Renvoie le sous-arbre (de type ABR)  gauche de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.gauche
    
    def element_racine(self):
        """Renvoie l'élément stocké dans le noeud racine de l'arbre 
        Provoque une erreur si arbre est vide"""
        assert not self.est_vide()
        return self.racine.element

    # extension de l'interface
    def recherche(self, elt):
        """
        Renvoie True si element dans l'arbre binaire de recherche
        et False sinon        
        """
        if self.est_vide(): # cas de l'arbre vide
            return False
        elif elt < self.element_racine():
            return self.gauche().recherche(elt)
        # à compléter   

# tests unitaires
def test_abr_mutable2():
    a = ABR2()
    a.racine = Noeud4(ABR2(), 6,  ABR2())
    b = ABR2()
    b.racine = Noeud4(ABR2(), 4, a)
    c = ABR2()
    c.racine = Noeud4(ABR2(), 9, ABR2())
    d = ABR2()
    d.racine = Noeud4(ABR2(), 12, ABR2())
    e = ABR2()
    e.racine = Noeud4(c, 10, d)
    f = ABR2()
    f.racine = Noeud4(b, 8, e)
    assert a.recherche(8) == True
    assert a.recherche(6) == True
    assert a.recherche(12) == True
    assert a.recherche(13) == False
    assert  a.recherche(7) == False
    print("Tests réussis")