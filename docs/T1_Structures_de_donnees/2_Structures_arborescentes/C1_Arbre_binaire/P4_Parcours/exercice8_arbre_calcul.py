class Noeud:
    """Noeud pour arbre binaire"""
    
    def __init__(self, g, e, d):
        self.gauche = g # lien vers fils gauche g éventuellement vide (None)
        self.element = e # élément e stocké dans le noeud
        self.droit = d # lien vers fils droit d éventuellement vide (None)
    
def parcours_infixe(arbre, trace):
    if arbre is None:
        return 
    parcours_infixe(arbre.gauche, trace)
    trace.append(str(arbre.element))
    parcours_infixe(arbre.droit, trace)
    
trace_infixe = []
arbre_calcul = a = Noeud(Noeud(Noeud(None, 3, None), '*', Noeud(Noeud(None, 4, None), '-', Noeud(None, 5, None))), 
                         '+', Noeud(Noeud(None, 5, None), '*', Noeud(Noeud(None, 12, None), '+', Noeud(None, 7, None))))
parcours_infixe(arbre_calcul, trace_infixe)
print(' '.join(trace_infixe))