---
title:  Définitions 🎯
---

#  Définitions  (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme_arbre.png){: .center}



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [manuel NSI](https://www.mesmanuels.fr/acces-libre/9782017189992) chez Hachette sous la direction de *Michel Beaudouin Lafon*
    * [l'article sur les arbres en NSI](https://sebhoa.gitlab.io/iremi/03_Didactique/arbres/) de Sébastien Hoarau
    * le cours de mon collègue Pierre Duclosson
    * le [cours de Gilles Lassus](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.1_Listes_Piles_Files/cours/)
    * le [cours de Franck Chambon](https://ens-fr.gitlab.io/nsi2/5-Lin%C3%A9aires/1-pile-0/)


 [🔖 Synthèse de ce qu'il faut retenir pour le bac](../Synthèse/synthese_arbre_binaire.md){: .md-button}

## Définitions


!!! note "Point de cours 1 : arbre binaire"
    Un **arbre binaire** est un ensemble fini de **noeuds**. Cet ensemble peut être vide. 
    
    Un  **noeud** d'arbre binaire  est constitué de  trois champs  :

    * un  champ d'information contient un *élément* 
    * un  premier champ *enfant* contient un lien  appelé *fils gauche*  vers un autre noeud  ou une valeur représentant l'absence de noeud
    * un  second champ *enfant*  contient un lien  appelé *fils droit* vers un autre noeud  ou une valeur représentant l'absence de noeud
    
    !!! warning "Un noeud d'arbre binaire a toujours deux enfants"
        Un noeud d'arbre binaire est normalement toujours représenté avec ses deux enfants même s'ils sont vides.
        
        ![alt](images/noeud_reMAX_SIZEd.png)
  
    On peut alors définir un **arbre binaire** de façon  *récursive* :

    * soit c'est **l'arbre vide** s'il ne contient aucun noeud
    * soit il est constitué d'un noeud spécial appelé **racine** de l'arbre dont le *fils droit* et le *fils gauche* sont des **arbres binaires**

    Le fils gauche non vide d'un arbre binaire non vide est appelé *sous-arbre gauche*.

    Le fils droit non vide d'un arbre binaire non vide est appelé *sous-arbre droit*.

    !!! warning "Distinction entre fils gauche et droit"
        Attention, il faut bien distinguer le fils gauche et le fils droit : il existe deux  structures d'arbre binaire distinctes avec deux noeuds.

        ![alt](images/arbres_binaires.png)

    !!! warning "Arbres binaires homogènes ou pas"
        En général, comme pour les structures linéaires, nous construirons des arbres binaires stockant des éléments de même type. Mais ce ne sera pas le cas pour les arbres de calcul comme celui-présenté dans [l'introduction des structures arborescentes](../../C0_Structure_arborescente/P0_Structure_arborescente.md).


!!! question "Exercice 1"

    [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/2291-1765269){: .md-button}


    === "énoncé"
        1. Représenter tous les arbres binaires ayant trois nœuds.
        2. Représenter tous les arbres binaires ayant quatre nœuds.

    === "solution"
        On a cinq arbres binaires avec trois noeuds.

        ![alt](images/catalan3.png)

        On a 14 arbres binaires avec quatre noeuds.

        ![alt](images/catalan4.png):

        Le nombre d'arbres binaires à $n$ noeuds est le $n^{ième}$ [nombre de Catalan](https://fr.wikipedia.org/wiki/Nombre_de_Catalan). Retrouver la formule de récurrence est un bon exercice de dénombrement. Ci-dessous une fonction pour renvoyer un tableau contenant tous les arbres binaires de taille $n$.

        ~~~python
        def enumeration_arbres(n):
            if n == 0:
                return [None]
            preced = [enumeration_arbres(k) for k in range(n)]
            tab = []
            for k in range(n):
                for sag in preced[k]:
                    for sad in preced[n - 1 - k]:
                        tab.append(Noeud(sag, '', sad))
            return tab
        ~~~


!!! note "Point de cours 2 : noeuds"
    L'accès aux éléments stockés dans  un **arbre binaire**  non vide s'effectue par le noeud spécial appelé **racine**.

    Il existe une unique chaîne reliant par des liens de filiation, le noeud racine  à un autre noeud.

    Un noeud qui a au moins un enfant est appelé **noeud interne**.

    Un noeud qui n'a aucun enfant (ni *fils gauche*, ni *fils droit*) est appelé **feuille**.

    ![alt](images/vocabulaire_arbre.png)


!!! question "Exercice 2"

    [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/2291-1765269){: .md-button}


    === "énoncé"
        1. Représenter tous les arbres binaires qui ont trois noeuds mais une seule feuille.
        2. Représenter tous les arbres binaires qui ont quatre noeuds mais une seule feuille.
        3. Combien existe-t-il d'arbres binaires qui ont $n \geqslant 1$ noeuds mais une seule feuille.

    === "solution"
        1. On a $1 \times 2 \times 2 = 4$ arbres binaires dégénérés avec trois noeuds et une seule feuille.
        2. On a $1 \times 2 \times 2 \times 2  = 8$ arbres binaires dégénérés avec trois noeuds et une seule feuille.
        3. Pour $n \geqslant 1$, on a $2^{n-1}$  arbres binaires dégénérés avec $n$ noeuds et une seule feuille. A chaque niveau supplémentaire on multiplie par $2$ le nombre de possibilités.




