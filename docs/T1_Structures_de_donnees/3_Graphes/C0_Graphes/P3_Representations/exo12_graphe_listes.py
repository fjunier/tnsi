class Graphe_liste:
    
    def __init__(self, liste_sommets):
        """
        Crée une représentation de  graphe non orienté à partir d'une liste de sommets
        numérotés de 0 à n - 1
        """
        self.liste_sommets = liste_sommets
        self.n = len(self.liste_sommets)
        self.adjacents = ...
        
    def sommets(self):
        """
        Renvoie une liste des sommets
        """
        return self.liste_sommets
    
    def ajoute_arc(self, sommetA, sommetB):
        """
        Ajoute dans la représentation de graphe l'arc sommetA - sommetB
        """
        assert (sommetA in self.liste_sommets), "sommet A pas dans le graphe"
        assert (sommetB in self.liste_sommets), "sommet B pas dans le graphe"
        # à compléter
    
    def voisins(self, sommet):
        """
        Renvoie une liste des voisins du sommet dans la représentation du graphe
        """
        assert sommet in self.liste_sommets, "sommet pas dans le graphe"
        return self.adjacents[sommet]

    def est_arc(self, sommetA, sommetB):
        """
        Renvoie un booléen indiquant si l'arc sommetA - sommetB appartient au graphe
        """
        assert sommetA in self.liste_sommets, "sommetA pas dans le graphe"
        # à compléter
    
    def export_matrice(self):
        """
        Renvoie une représentation du graphe sous forme de matrice  d'adjacence
        """
        # à compléter
        
    

def test_graphe_liste():
    """
    Tests unitaires pour la classe Graphe_matrice
    """
    g1 = Graphe_liste([0, 1, 2, 3, 4])
    g1.ajoute_arc(1, 2)
    g1.ajoute_arc(1, 4)
    g1.ajoute_arc(2, 3)
    g1.ajoute_arc(2, 4)
    g1.ajoute_arc(3, 4)
    g1.ajoute_arc(4, 0)
    assert g1.est_arc(1, 2) == True, "échec sur g1.est_voisin(1, 2)"
    assert g1.est_arc(2, 1) == True, "échec sur g1.est_voisin(2, 1)"
    assert g1.sommets() == [0, 1, 2, 3, 4], "échec sur g1.sommets()"
    assert g1.voisins(2) == [1, 3, 4], "échec sur g1.voisins(2)"
    assert g1.voisins(1) == [2, 4], "échec sur g1.voisins(1)"
    assert g1.export_matrice() == [[0, 0, 0, 0, 1], [0, 0, 1, 0, 1], 
                                   [0, 1, 0, 1, 1], [0, 0, 1, 0, 1],
                                   [1, 1, 1, 1, 0]], "échec sur g1.export_matrice()"
    print("Tests réussis")
