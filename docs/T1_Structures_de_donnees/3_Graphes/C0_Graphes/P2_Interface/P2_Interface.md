---
title:  Interface 🎯
---

#  Interface (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme_graphe.png){: .center}



!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [manuel NSI](https://www.nsi-terminale.fr/) chez Ellipses de *Balabonski, Conchon, Filliâtre, Nguyen*
    * le [cours de Gilles Lassus](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.4_Graphes/cours/)
    * le [cours de Cédric Gouygou](https://cgouygou.github.io/TNSI/T01_StructuresDonnees/T1.5_Graphes/T1.5.1_Graphes/)



 [🔖 Synthèse de ce qu'il faut retenir pour le bac](../Synthèse/synthese_graphe.md){: .md-button}

## Interface objet


!!! note "Point de cours 3 : une interface pour une classe Graphe"
    Pour implémenter les algorithmes de graphe au programme, l'interface minimale ci-dessous sera suffisante. Le choix d'une structure de données de  représentation  du graphe dépend de l'implémentation choisie et sera développé dans la section suivante.

    L'interface est donnée pour un *graphe orienté*,  pour un *graphe non orienté* une petite adaptation sera nécessaire :

    * Dans la méthode `ajoute_arc`, par symétrie,  il faudra penser à ajouter les arcs sommetA -> sommetB et sommetB -> sommetA dans la représentation interne du graphe.


    ~~~python
    class Graphe:
    
        def __init__(self, liste_sommets):
            """
            Crée une représentation de  graphe orienté à partir d'une liste de sommets
            """

        def sommets(self):
            """
            Renvoie une liste des sommets
            """

        def ajoute_arc(self, sommetA, sommetB):
            """
            Ajoute dans la représentation de graphe l'arc sommetA -> sommetB
            """

        def voisins(self, sommet):
            """
            Renvoie une liste des voisins du sommet dans la représentation du graphe
            """

        def est_arc(self, sommetA, sommetB):
            """
            Renvoie un booléen indiquant si l'arc sommetA - sommetB appartient au graphe
            """
    ~~~

## Application de l'interface objet


!!! question "Exercice 3"

    

    !!! success "Question 1"

        ![alt](images/graphe_cours2.png)

        === "énoncé"
            Compléter la séquence d'instructions ci-dessous pour représenter le graphe orienté  ci-dessus comme instance de la classe `Graphe` :

            ~~~python
            g1 = Graphe([0, 1, 2, 3, 4])
            ~~~

        === "solution"

            ~~~python
            g1 = Graphe([0, 1, 2, 3, 4])
            g1.ajoute_arc(1, 2)
            g1.ajoute_arc(1, 4)
            g1.ajoute_arc(2, 3)
            g1.ajoute_arc(2, 4)
            g1.ajoute_arc(3, 4)
            g1.ajoute_arc(4, 0)
            ~~~

    !!! success "Question 2"
        === "énoncé"

            Étendre l'interface de la classe `Graphe` précédente, dans le cas où elle  modélise un graphe orienté, avec une  méthode :

            * `degre_sortant(self, sommet)` qui renvoie le degré sortant d'un sommet

        === "solution"

            ~~~python    
            def degre_sortant(self, sommet):
                """
                Renvoie le degré sortant d'un sommet du graphe orienté
                """
                return len(self.voisins(sommet))
            ~~~

    !!! success "Question 3"
        === "énoncé"

            Étendre l'interface de la classe `Graphe` précédente, dans le cas où elle  modélise un graphe orienté, avec une  méthode :

            * `degre_entrant(self, sommet)` qui renvoie le degré entrant d'un sommet

        === "solution"

            ~~~python    
            def degre_entrant(self, sommet):
                """
                Renvoie le degré entrant d'un sommet du graphe orienté
                """
                d = 0
                for s in self.sommets():
                    if sommet in self.voisins(s):
                        d = d + 1
                return d
            ~~~