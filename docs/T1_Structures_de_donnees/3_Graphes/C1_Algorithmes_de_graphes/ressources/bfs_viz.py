import graphviz
import time
import imageio
import PIL
import copy
from collections import deque

def file_vide():
    return deque([])

def est_file_vide(file):
    return file == file_vide()

def enfiler(valeur, file):
    file.append(valeur)
    
def defiler(file):
    return file.popleft()


def liste_adjacence_vers_csv(graphe, chemin, encodage="utf-8"):
    """
    Export d'un graphe vers un fichier csv.

    Graphe sous forme de tableau de listes d'adjacences
     dont chaque ligne est de la forme :

    sommet;voisin1;voisin2;voisin3

    avec sommet le numéro de la ligne

    Parameters
    ----------
    graphe : représentation d'un graphe par un tableau de listes d'adjacences
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    None.

    """
    f = open(chemin, 'w', encoding=encodage)
    for sommet in range(len(graphe)):
        if len(graphe[sommet]) == 0:
            ligne = str(sommet) + ';' + '\n'
        else:
            ligne = ';'.join([str(sommet)] + [str(v) for v in graphe[sommet]]) + '\n'
        f.write(ligne)
    f.close()


def matrice_adjacence_vers_csv(graphe, chemin, encodage="utf-8"):
    """
    Export d'un graphe  vers un fichier csv.

    Graphe sous forme de matrice d'adjacence
    dont la  ligne numéro i contient autant de champs
    que de sommets dans le graphe. Le champ j vaut 1 ou 0
    (valeur de  graphe[i][j])

    Parameters
    ----------
    graphe : représentation d'un graphe par une matrice d'adjacence
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    None.

    """
    f = open(chemin, 'w', encoding=encodage)
    for i in range(len(graphe)):
        f.write(';'.join([str(graphe[i][j]) for j in range(len(graphe))]) + '\n')
    f.close()


def dico_adjacence_vers_csv(graphe, chemin, encodage="utf-8"):
    """
    Export d'un graphe  vers un fichier csv.

    Graphe sous forme de dictionnaire de listes d'adjacences
    Fichier csv dont chaque ligne est de la forme :

    sommet;voisin1;voisin2;voisin3

    Parameters
    ----------
    graphe :représentation de graphe par un dictionnaire de listes d'adjacences
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    None.

    """
    f = open(chemin, 'w', encoding=encodage)
    for sommet in graphe:
        if len(graphe[sommet]) == 0:
            ligne = str(sommet) + ';' + '\n'
        else:
            ligne = ';'.join([str(sommet)] + [str(v) for v in graphe[sommet]]) + '\n'
        f.write(ligne)
    f.close()


def csv_vers_liste_adjacence(chemin, encodage="utf-8"):
    """
    Import d'un graphe depuis un fichier csv.

    Graphe sous forme de tableau de listes d'adjacences
    Fichier csv dont chaque ligne est de la forme :

    sommet;voisin1;voisin2;voisin3

    Parameters
    ----------
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    graphe : représentation d'un graphe par un tableau de listes d'adjacences
    """
    f = open(chemin, encoding=encodage)
    les_lignes = f.readlines()
    nb_sommets = len(les_lignes)
    graphe = [[] for _ in range(nb_sommets)]
    for ligne in les_lignes:
        champs = ligne.rstrip().split(';')
        if champs[1] != '':
            graphe[int(champs[0])] = [int(v) for v in champs[1:]]
        else:
            graphe[int(champs[0])] = []
    f.close()
    return graphe


def csv_vers_matrice_adjacence(chemin, encodage="utf-8"):
    """
    Import d'un graphe depuis un fichier csv.

    Graphe sous forme de matrice d'adjacence.
    Dans le  fichier csv la  ligne numéro i contient autant de champs
    que de sommets dans le graphe. Le champ j vaut 1 ou 0
    (valeur de  graphe[i][j])

    Parameters
    ----------
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    graphe : représentation d'un graphe par une matrice d'adjacence
    """
    f = open(chemin, encoding=encodage)
    les_lignes = f.readlines()
    nb_sommets = len(les_lignes)
    graphe = [[0 for _ in range(nb_sommets)] for _ in range(nb_sommets)]
    for i in range(nb_sommets):
        ligne = les_lignes[i]
        champs = ligne.rstrip().split(';')
        for j in range(nb_sommets):
            graphe[i][j] = int(champs[j])
    f.close()
    return graphe


def csv_vers_dico_adjacence(chemin, encodage="utf-8"):
    """
    Import d'un graphe depuis un fichier csv.

    Graphe sous forme de dictionnaire de listes d'adjacences
    dont chaque ligne est de la forme :

    Parameters
    ----------
    chemin (str) : chemin d'accès au fichier d'enregistrement
    encodage  (str): encodage
        DESCRIPTION. The default is "utf-8".

    Returns
    -------
    graphe : représentation d'un graphe par un dictionnaire de listes
    d'adjacences
    """
    graphe = dict()
    f = open(chemin, encoding=encodage)
    for ligne in f:
        champs = ligne.rstrip().split(';')
        if champs[1] != '':
            graphe[champs[0]] = champs[1:]
        else:
            graphe[champs[0]] = []
    f.close()
    return graphe


# %% Import/export de graphe vers fichier binaire (sérialisatio)
import pickle


def enregistrer_graphe(graphe, chemin):
    """
    Enregistre  graphe dans un fichier binaire   de chemin d'accès chemin.

    Paramètres :
        graphe : représentation d'un graphe (tablau de listes d'adjacences
                 , dictionnaire de listes d'adjacences ...)
        chemin (str) : chemin d'accès au fichier d'enregistrement
    """
    f = open(chemin, "wb")
    pickle.dump(graphe, f)
    f.close()


def charger_graphe(chemin):
    """
    Charge  un graphe stocké dans un fichier binaire de chemin d'accès chemin.

    Paramètres :
        chemin (str) : chemin d'accès au fichier d'enregistrement

    Retour :
        graphe : représentation d'un graphe (tableau de listes d'adjacences
                 , dictionnaire de listes d'adjacences ...)
    """
    f = open(chemin, "rb")
    graphe = pickle.load(f)
    f.close()
    return graphe


# %% Export de graohe vers le format dot pour visualisation avec graphviz
import copy


def liste_adjacence_vers_dot(graphe, sortie="graphe_dot.dot", large=False,
                             digraph=False):
    """
    Export d'un graphe vers un fichier texte et une string au format dot.

    Graphe représenté par un tableau de  listes  d'adjacences
    vers un fichier texte et une chaine de caractères au format dot
    coller la chaine dans https://dreampuf.github.io/GraphvizOnline
    pour visualiser

    Parameters
    ----------
    graphe : représentation d'un graphe sous forme de tableau de  listes
                d'adjacences
             sommets numérotés par des entiers à partir de 0
    sortie (str) : chemin vers un fichier texte de sortie
    large (boolean) : graphe large ou non
    digraph (boolean) : graphe orienté ou non

    Returns
    -------
    contenu (str) : chaine de caractères,
                représentation du graphe au format dot
    """
    graphe_copie = copy.deepcopy(graphe)
    lignes = ["digraph {"]
    if large:
        lignes.append("layout=sfdp;")
    if not digraph:
        lignes.append("edge [dir = none];")
    lignes.append("rankdir=LR;")
    for i in range(len(graphe_copie)):
        if graphe_copie[i] == []:
            lignes.append(str(i)+";")
        for j in graphe_copie[i]:
            lignes.append(str(i)+"->"+str(j)+";")
            if not digraph:
                graphe_copie[j].remove(i)
    lignes.append("}")
    contenu = "\n".join(lignes)
    f = open(sortie, "w", encoding="utf-8")
    f.write(contenu)
    f.close()
    return contenu.replace('\n', '')


def dico_adjacence_vers_dot(graphe, sortie="graphe_dot.dot", large=False,
                            digraph=False):
    """
    Export d'un graphe vers un fichier texte et une string au format dot.

    Graphe représenté par un dictionnaire de  listes  d'adjacences.
    coller la chaine dans https://dreampuf.github.io/GraphvizOnline
    pour visualiser

    Parameters
    ----------
    graphe : représentation d'un graphe sous forme de dictionnaire de
            listes  d'adjacences  d'adjacences
             sommets numérotés par des entiers à partir de 0
    sortie (str) : chemin vers un fichier texte de sortie
    large (boolean) : graphe large ou non
    digraph (boolean) : graphe orienté ou non

    Returns
    -------
    contenu (str) : chaine de caractères,
                représentation du graphe au format dot
    """
    graphe_copie = copy.deepcopy(graphe)
    lignes = ["digraph {"]
    if large:
        lignes.append("layout=sfdp;")
    if not digraph:
        lignes.append("edge [dir = none];")
    lignes.append("rankdir=LR;")
    for sommet in graphe_copie:
        if graphe_copie[sommet] == []:
            lignes.append('"' + str(sommet) + '"' + ";")
        for v in graphe_copie[sommet]:
            lignes.append('"' + str(sommet) + '"' + "->" + '"' + str(v) + '"' + ";")
            if not digraph:
                graphe_copie[v].remove(sommet)
    lignes.append("}")
    contenu = "\n".join(lignes)
    f = open(sortie, "w", encoding="utf-8")
    f.write(contenu)
    f.close()
    return contenu.replace('\n', ' ')



def trace_bfs_viz(graphe, sommet, sortie, delai = 1, zoom = 1):
    
    def bfs_viz(sommet, graphe, marque, parent, distance):
        """
        dfs récursif à partir de sommet
        graphe liste ou dico d'adjacence
        """
        nb_sommets = len(graphe)
        file = file_vide()
        marque[sommet] = True
        distance[sommet] = 0
        enfiler(sommet, file)
        while not est_file_vide(file):
            courant = defiler(file)
            if not trace["invisible"]:
                nouvelle_trace = f"{courant} [color = red]; \n"
                trace["data"] = trace["data"] + nouvelle_trace
            for v in graphe[courant]:
                if not marque[v]:
                    # on pourrait se passer de marque c'est pour avoir la même signature pour tous les parcours
                    distance[v] = distance[courant] + 1
                    marque[v] = True 
                    parent[v] = courant
                    enfiler(v, file)
                    if trace["invisible"]:
                        nouvelle_trace =  f"{courant} -> {v}[style = invis, comment = \"{trace['compteur']}\", fontcolor = white, dir=right, label={distance[v]}]; \n"
                        trace["data"] = trace["data"] + nouvelle_trace
                    else:
                        trace["data"] = trace["data"].replace(f"style = invis, comment = \"{trace['compteur']}\", fontcolor = white", f"style = bold, color=green, fontcolor = green")                
                        trace["data"] = trace["data"] + f"{v} [color = green]; \n"
                    #print(trace["data"])
                    if not trace["invisible"]:                    
                        chemin = f"{trace['sortie']}-{trace['compteur']}.dot"
                        f = open(chemin, "w", encoding="utf-8")                
                        f.write(trace["data"] + "}")
                        f.close()
                        graphviz.render('dot', 'png', chemin)
                        graphviz.Source.from_file(chemin).view()
                        time.sleep(delai)
                        img = imageio.imread(chemin + ".png")
                        if trace["compteur"] == 1:
                            trace["hauteur"], trace["largeur"], _ = img.shape
                            trace["hauteur"] = zoom * trace["hauteur"]
                            trace["largeur"] = zoom * trace["largeur"]
                        img = PIL.Image.fromarray(img).resize((trace["largeur"], trace["hauteur"]))
                        trace["animation"].append(img)
                    trace["compteur"] += 1
            #print(trace["data"])
            if not trace["invisible"]:                    
                chemin = f"{trace['sortie']}-{trace['compteur']}.dot"
                f = open(chemin, "w", encoding="utf-8")                
                f.write(trace["data"] + "}")
                f.close()
                graphviz.render('dot', 'png', chemin)
                graphviz.Source.from_file(chemin).view()
                time.sleep(delai)
                img = imageio.imread(chemin + ".png")
                if trace["compteur"] == 1:
                    trace["hauteur"], trace["largeur"], _ = img.shape
                    trace["hauteur"] = zoom * trace["hauteur"]
                    trace["largeur"] = zoom * trace["largeur"]
                img = PIL.Image.fromarray(img).resize((trace["largeur"], trace["hauteur"]))
                trace["animation"].append(img)
            trace["compteur"] += 1
        return distance, parent
        
        
    if isinstance(graphe, list):
        trace = {"data": liste_adjacence_vers_dot(graphe).rstrip('}'),
                 "sortie": sortie,
                 "compteur": 1,
                 "animation":[],
                 "invisible": True
                }
        marque = [False for _ in range(len(graphe))]
        parent = [None for _ in range(len(graphe))]
        distance = [float('inf') for s in range(len(graphe))]
    else: # pas testé
        trace = {"data": dico_adjacence_vers_dot(graphe)[:-1].rstrip('}'),
                 "sortie": sortie,
                 "compteur": 1,
                 "animation":[],
                 "invisible": True
                }
        marque = {s: False for s in graphe}
        parent = {s: None for s in graphe}
        distance = {s: float('inf') for s in graphe}
    # on lance un premier parcours en largeur pour préparer le graphique avec des arcs invisibles
    trace["invisible"] = True
    bfs_viz(sommet,  graphe, marque, parent, distance)
    trace["invisible"] = False
    if isinstance(graphe, list):
        marque = [False for _ in range(len(graphe))]
        parent = [None for _ in range(len(graphe))]
        distance = [float('inf') for s in range(len(graphe))]
    else: # pas testé
        marque = {s:False for s in graphe}
        parent = {s: None for s in graphe}
        distance = {s: float('inf') for s in graphe}
    trace["compteur"] = 1
    bfs_viz(sommet,  graphe, marque, parent, distance)
    # pour l'animation
    imageio.mimwrite(sortie + "trace-bfs-animation.gif", trace["animation"],  format= '.gif', fps = 2)


def test_unitaire_bfs_viz():
    graphe1 = [[1, 2, 6], [0, 3, 5, 6], [0, 3], [1, 2, 4], [3, 5, 8], [1, 4, 7], [0, 1], [5, 8], [4, 7]]
    trace_bfs_viz(graphe1, 0, "test_bfs_viz/graphe1/graphe1-trace", 0.1, zoom = 2)
    graphe_routes = {'JFK': ['MCO', 'ATL', 'ORD'],
                         'MCO': ['JFK', 'ATL', 'HOU'],
                         'ORD': ['DEN', 'HOU', 'DFW', 'PHX', 'JFK', 'ATL'],
                         'DEN': ['ORD', 'PHX', 'LAS'],
                         'HOU': ['ORD', 'ATL', 'DFW', 'MCO'],
                         'DFW': ['PHX', 'ORD', 'HOU'],
                         'PHX': ['DFW', 'ORD', 'DEN', 'LAX', 'LAS'],
                         'ATL': ['JFK', 'HOU', 'ORD', 'MCO'],
                         'LAX': ['PHX', 'LAS'],
                         'LAS': ['DEN', 'LAX', 'PHX']}
    trace_bfs_viz(graphe_routes, 'JFK', "test_bfs_viz/graphe_routes/graphe-routes", 0.1, zoom = 2)
    
#test_unitaire_bfs_viz()
#graphe_cours = csv_vers_liste_adjacence("graphes/graphe_cours_dfs.csv")
#trace_bfs_viz(graphe_cours, 0, "graphes/animation_bfs/graphe_cours_bfs-trace")

# graphe exemple tim rouhgarden "illuminated algorithms"

graphe = {"s": ["a", "b"], "a": ["s", "c"], "b": ["s", "c", "d"], 
          "c": ["a", "d", "b", "e"],
          "d": ["b", "c", "e"], "e": ["c", "d"]}
trace_bfs_viz(graphe, "s", "trace_bfs/trace_roughgarden",1, zoom=2)