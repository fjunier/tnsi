# génération du graphe

individus = ['Pikachu', 'Mewtwo', 'Link', 'Zelda', 'Sheik', 'Ganondorf', 'Snake', 'Samus', 'Samus sans armure',
 'Dark Samus', 'Ridley', 'Fox', 'Wolf', 'Falco', 'Diddy Kong', 'King K. Rool', 'Donkey Kong', 'Mario',
 'Sonic', 'Wario', 'Luigi', 'Bowser', 'Peach', 'Yoshi', 'Daisy']
smashbros = Graphe(individus)
liste_arcs = [('Pikachu', 'Mewtwo'), ('Mewtwo', 'Pikachu'), ('Link', 'Ganondorf'), ('Zelda', 'Ganondorf'), ('Sheik', 'Ganondorf'), ('Ganondorf', 'Link'), ('Ganondorf', 'Zelda'), ('Ganondorf', 'Sheik'), ('Samus', 'Dark Samus'), ('Samus', 'Ridley'), ('Samus sans armure', 'Dark Samus'), ('Samus sans armure', 'Ridley'), ('Dark Samus', 'Samus'), ('Dark Samus', 'Samus sans armure'), ('Ridley', 'Samus'), ('Ridley', 'Samus sans armure'), ('Fox', 'Wolf'), ('Wolf', 'Fox'), ('Wolf', 'Falco'), ('Falco', 'Wolf'), ('Diddy Kong', 'King K. Rool'), ('King K. Rool', 'Diddy Kong'), ('King K. Rool', 'Donkey Kong'), ('Donkey Kong', 'King K. Rool'), ('Donkey Kong', 'Mario'), ('Mario', 'Donkey Kong'), ('Mario', 'Sonic'), ('Mario', 'Bowser'), ('Mario', 'Wario'), ('Sonic', 'Mario'), ('Wario', 'Mario'), ('Wario', 'Luigi'), ('Luigi', 'Wario'), ('Luigi', 'Bowser'), ('Bowser', 'Mario'), ('Bowser', 'Luigi'), ('Bowser', 'Peach'), ('Bowser', 'Daisy'), ('Bowser', 'Yoshi'), ('Peach', 'Bowser'), ('Yoshi', 'Bowser'), ('Daisy', 'Bowser')]
for i, j in liste_arcs:
    smashbros.ajoute_arc(i, j) 


def dfs_rec_cc(sommet, graphe, decouvert, composante, numero):
    """
    Parcours en profondeur qui marque dans un dictionnaire composante
    le numero de composante des sommets découverts
    """
    # à compléter
 

def composantes_connexes(graphe):
    """
    Numérote à partir de 1 les composantes connexes 
    d'un graphe non orienté dans un dictionnaire composante    
    """
    decouvert = {s: False for s in graphe.sommets()}
    composante = {s: -1 for s in graphe.sommets()}
    numero = 1
    for s in graphe.sommets():
        if not decouvert[s]:
            dfs_rec_cc(s, graphe, decouvert, composante, numero)
            numero += 1
    return composante

def test_composantes_connexes():
    """Test unitaire pour composantes_connexes"""
    attendu = {'Pikachu': 1, 'Mewtwo': 1, 'Link': 2, 'Zelda': 2, 'Sheik': 2, 'Ganondorf': 2, 'Snake': 3,
             'Samus': 4, 'Samus sans armure': 4, 'Dark Samus': 4, 'Ridley': 4, 'Fox': 5, 'Wolf': 5, 'Falco': 5,
             'Diddy Kong': 6, 'King K. Rool': 6, 'Donkey Kong': 6, 'Mario': 6, 'Sonic': 6, 'Wario': 6, 'Luigi': 6,
             'Bowser': 6, 'Peach': 6, 'Yoshi': 6, 'Daisy': 6}
    assert composantes_connexes(smashbros)
    print("Tests réussis")

# Exécutez d'abord le code de l'éditeur de l'exercice 15

