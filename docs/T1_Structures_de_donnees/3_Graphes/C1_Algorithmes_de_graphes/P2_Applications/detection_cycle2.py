def construire_cycle(sommet, graphe, precedent):
    """
    Reconstruit un cycle d'extrémités sommet
    dans un graphe orienté
    à partir du dictionnaire precedent 
    associant à chaque sommet découvert son prédécesseur
    dans un parcours dfs
    """
    cycle = [sommet]
    courant = precedent[sommet]
    # à compléter
    return cycle



def dfs_cycle2(sommet, graphe, decouvert, en_cours, precedent):
    """
    Parcours en profondeur récursif augmenté pour marquer :
    - dans  le dictionnaire decouvert  que sommet est découvert au cours du parcours
    - dans le dictionnaire en_cours que sommet est en cours de parcours dfs au début de l'appel
    et en fin de parcours dfs à la fin de l'appel
    - dans le dictionnaire precedent le prédécesseur du sommet découvert 
    
    Renvoie une liste Python :  : le cycle ou une liste vide
    """
    decouvert[sommet] = True
    en_cours[sommet] = True
    for v in graphe.voisins(sommet):
        if not decouvert[v]:
            precedent[v] = sommet
            rep = dfs_cycle2(v, graphe, decouvert, en_cours, precedent)
            # à compléter
        elif en_cours[v]:
            precedent[v] = sommet
            # à compléter
            return ...
    en_cours[sommet] = False
    return []

def detecter_cycle2(graphe):
    """
    Détermine si  le graphe 
    orienté contient un cycle
    Renvoie  une liste Python : le cycle ou une liste vide
    """
    decouvert = {s: False for s in graphe.sommets()}    
    precedent = {s:None for s in graphe.sommets()}
    # à compléter
    ...
    return []
    

    
    
def test_detecter_cycle2():
    """Test unitaire pour detester_cycle"""
    assert detecter_cycle2(graphe1) == ['module_B', 'module_C', 'module_E', 'module_F', 'module_A', 'module_B'], "échec sur le graphe de dépendances 1"
    assert detecter_cycle2(graphe2) == [], "échec sur le graphe de dépendances 2"
    print("Tests réussis")