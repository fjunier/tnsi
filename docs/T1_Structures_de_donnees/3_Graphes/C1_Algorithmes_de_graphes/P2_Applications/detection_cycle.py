class Graphe_oriente:
    
    def __init__(self, liste_sommets):
        """
        Crée une représentation de  graphe  orienté à partir d'une liste de sommets
        """
        self.liste_sommets = liste_sommets
        self.adjacents = {sommet : [] for sommet in liste_sommets}
        
    def sommets(self):
        """
        Renvoie une liste des sommets
        """
        return self.liste_sommets

    def ajoute_arc(self, sommetA, sommetB):
        """
        Ajoute dans la représentation de graphe l'arc sommetA -> sommetB
        """
        assert (sommetA in self.liste_sommets), "sommet A pas dans le graphe"
        assert (sommetB in self.liste_sommets), "sommet B pas dans le graphe"
        self.adjacents[sommetA].append(sommetB)


    def voisins(self, sommet):
        """
        Renvoie une liste des voisins du sommet dans la représentation du graphe
        """
        assert sommet in self.liste_sommets, "sommet pas dans le graphe"
        return self.adjacents[sommet]

    def est_arc(self, sommetA, sommetB):
        """
        Renvoie un booléen indiquant si l'arc sommetA -> sommetB appartient au graphe
        """
        assert sommetA in self.liste_sommets, "sommetA pas dans le graphe"
        return sommetB in self.adjacents[sommetA]
    
    def supprime_sommet(self, sommet):
        """
        Supprime sommet du graphe
        """
        self.liste_sommets.remove(sommet)
        del self.adjacents[sommet]
        for s in self.sommets():
            if sommet in self.voisins(s):
                self.adjacents[s].remove(sommet)  
    
    def degre_entrant(self, sommet):
        """
        Renvoie le degré entrant d'un sommet du graphe orienté
        """
        d = 0
        for s in self.sommets():
            if sommet in self.voisins(s):
                d = d + 1
        return d
    
    def degre_sortant(self, sommet):
        """
        Renvoie le degré sortant d'un sommet du graphe orienté
        """
        d = 0
        for s in self.voisins(sommet):
            d = d + 1
        return d


def dfs_cycle(sommet, graphe, decouvert, en_cours):
    """
    Parcours en profondeur récursif augmenté pour marquer
    dans  le dictionnaire decouvert  que sommet est découvert au cours du parcours
    dans le dictionnaire en_cours que sommet est en cours de parcours dfs au début de l'appel
    et en fin de parcours dfs à la fin de l'appel
    Renvoie un booléen
    """
    decouvert[sommet] = True
    en_cours[sommet] = True
    # à compléter
    en_cours[sommet] = False
    return False

def detecter_cycle(graphe):
    """
    Renvoie un booléen indiquant si  le graphe 
    orienté contient un cycle
    """
    decouvert = {s: False for s in graphe.sommets()}    
    for s in graphe.sommets():
        if not decouvert[s]:
            en_cours = {s: False for s in graphe.sommets()}
            # à compléter
    return False


# génération du graphe de dépendances 1
liste_sommets1 = ['complex', 'module_B',  'module_E', 'module_F', 'module_D', 'math', 'cmath',  'module_C', 'module_A',  'module_G']
liste_arcs1 = [('complex', 'module_B'), ('complex', 'module_E'), ('module_B', 'module_A'), ('module_B', 'module_D'), ('module_E', 'module_C'), ('module_F', 'module_E'), ('module_D', 'module_E'), ('math', 'module_G'), ('cmath', 'module_A'), ('module_C', 'module_B'), ('module_A', 'module_F'), ('module_A', 'module_G')]
graphe1 = Graphe_oriente(liste_sommets1)
for i, j in liste_arcs1:
    graphe1.ajoute_arc(i, j)

# génération du graphe de dépendances 2
liste_sommets2 =  ['complex', 'module_B',  'module_E',  'module_G',  'module_F',  'math',  'cmath',  'module_C',  'module_A']
liste_arcs2 = [('complex', 'module_B'), ('complex', 'module_E'), ('module_B', 'module_C'), ('module_B', 'module_E'), ('module_B', 'module_G'), ('module_E', 'module_F'), ('module_G', 'module_A'), ('module_F', 'module_C'), ('math', 'module_A'), ('cmath', 'module_A'), ('module_C', 'module_A')]
graphe2 = Graphe_oriente(liste_sommets2)
for i, j in liste_arcs2:
    graphe2.ajoute_arc(i, j)

def test_detecter_cycle():
    """Test unitaire pour detester_cycle"""
    assert detecter_cycle(graphe1) == True, "échec sur le graphe de dépendances 1"
    assert detecter_cycle(graphe2) == False, "échec sur le graphe de dépendances 2"
    print("Tests réussis")

