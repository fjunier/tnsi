---
title:  Structure linéaire 🎯
---

#  Structure  linéaire (Bac 🎯)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


![programme](images/programme.png){: .center}

!!! note "Point de cours"
    Une **structure de données linéaire**  (ou **structure de données séquentielle**) est une forme d'organisation de données qui doivent être traitées de façon *séquentielle* :

    * une liste de données d'une série statistique : notes, températures ...
    * une file d'attente de processus en attente d'exécution
    * une file d'attente  d'utilisateurs en attente d'accès à un service
    * une pile des pages explorées précédemment au cours d'une navigation Web
    * la pile des contextes des appels imbriqués d'une fonction récursive  etc...

    Le programme de Terminale NSI introduit la notion de [type abstrait de données](./C1_Types_Abstraits_Listes/P1_Type_Abstrait/P1_Type_Abstrait.md) qui établit une distinction entre *l'interface* d'un type de données qui est offerte à l'utilisateur et son *implémentation* qui peut être dissimulée.

    On présente dans cette partie du programme trois types abstraits de **structures de données linéraires** :

    * le [type Liste](../C1_Types_Abstraits_Listes/P2_Liste/P2_Liste.md)
    * le [type Pile](../C2_Piles/P1_Interface/P1_Interface.md)
    * le [type File](../C3_Files/P1_Interface/P1_Interface.md)

