class Cellule2:
    
    def __init__(self, elt, suivant):
        self.element = elt
        self.suivant = suivant
        
class Pile:
    
    def __init__(self):
        self.contenu = None

    def pile_vide(self):
        return self.contenu is None

    def depiler(self):
        assert not self.pile_vide(), "Pile Vide"
        sommet = self.contenu.element
        self.contenu = self.contenu.suivant
        return sommet

    def empiler(self, elt):
        if self.pile_vide():
            self.contenu = Cellule2(elt, None)
        else:
            self.contenu = Cellule2(elt, self.contenu)
            
    def queue(self):
        assert not self.pile_vide()           
        pile_queue = Pile()
        pile_queue.contenu = self.contenu.suivant
        return pile_queue
    
    def __str__(self):
        if self.pile_vide():
            return 'None'
        return f"({str(self.contenu.element)},{str(self.queue())})"
    

class File3:
    
    def __init__(self):
        self.entree = Pile()
        self.sortie = Pile()
        
    def file_vide(self):
        return self.entree.pile_vide() and self.sortie.pile_vide()

    def defiler(self):
        assert not self.file_vide(), "File Vide"
        # à compléter
        ...          
      
    def enfiler(self, elt):
        # à compléter
        ...
        
    
    # interface étendue
    def __str__(self):
        sortie = "debut : " 
        tmp = File3()
        while not self.file_vide():
            elt = self.defiler()
            sortie = sortie + str(elt) + ' - '
            tmp.enfiler(elt)
        while not tmp.file_vide():
            self.enfiler(tmp.defiler())        
        sortie = sortie.rstrip(' - ') + ': fin'
        return sortie
    

def test_file3():
    f = File3()
    for k in range(1, 6):
        f.enfiler(k)
    for k in range(1, 6):
        assert f.defiler() == k
    assert f.file_vide()
    print("Tests réussis")