def creer_liste():
    #à compléter
    ...

def liste_vide(lis):
    return len(lis) == 0

def inserer(lis, elt):
    #à compléter
    ...

def tete(lis):
    #à compléter
    ...

def queue(lis):
    return [lis[k] for k in range(0, len(lis) - 1)]

def test():
    lis1 = creer_liste()
    inserer(lis1, 10)
    inserer(lis1, 9)
    assert tete(lis) == 9
    lis2 = queue(lis1)
    assert tete(lis2) == 10
    print("Tests réussis")