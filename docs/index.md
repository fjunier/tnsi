---
title: Progression
---

??? bug "Septembre"

    ??? done "Séance 1 : mardi 5/09/2023"

        * Consignes matérielles :
            * deux  favoris dans son navigateur :
                *  vers le site <https://fjunier.forge.aeif.fr/terminale_nsi>
                *  vers le [cours Moodle](https://0690026d.moodle.ent.auvergnerhonealpes.fr/course/view.php?id=63)
            * connexion à l'ENT avec Educonnect pour le service Capytale et pour Moodle
            * un cahier avec un côté pour les exercices de Cours et l'autre pour les exercices de TP
            * un classeur à la maison pour ranger les synthèses de cours

        * Séance du jour :
            * 🚴 Automatismes à faire dans ce [notebook Capytale](https://capytale2.ac-paris.fr/web/c/4976-1772633):
                * Exercices sur les dictionnaires :
                    * [Anniversaires](https://e-nsi.gitlab.io/pratique/N1/300-anniversaires/sujet/)
                    * [Valeurs extrêmes](https://e-nsi.gitlab.io/pratique/N1/300-dict_extremes/sujet/)
            * 📅 Chapitre traité : [POO](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/C1_POO/cours_POO/)
                * 📖 Cours :
                    * Partie 1 : [Structurer avec des classes](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/POO/P1_Stucturer_avec_des_classes/P1_cours_POO/)
            * Mini-projet : _logins réseau_
                * suivez le [guide](./Projets/mini_projet_logins/guide_eleve.md)
        * Pour jeudi :
            * Faire dans ce [notebook Capytale](https://capytale2.ac-paris.fr/web/c/2f14-1772634) les exercices :
                * <https://e-nsi.gitlab.io/pratique/N1/230-delta_encoding/sujet/>
                * <https://e-nsi.gitlab.io/pratique/N1/320-dico_occurrences/sujet/>

    ??? done "Séance 2 : jeudi 7/09/2023"

        * Séance du jour :        
            * 💯 Correction dans ce [notebook Capytale](https://capytale2.ac-paris.fr/web/c/2f14-1772634) des exercices :
                * <https://e-nsi.gitlab.io/pratique/N1/230-delta_encoding/sujet/>
                * <https://e-nsi.gitlab.io/pratique/N1/320-dico_occurrences/sujet/>
            * 📅 Chapitre traité : [POO](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/POO/P1_Stucturer_avec_des_classes/P1_cours_POO/)
                * 📖 Cours :
                    * Partie 2 : [Paradigme objet](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/C1_POO/cours_POO/)
            * Le point sur le vocabulaire de la POO, [QCM p. 41 du manuel](https://mesmanuels.fr/acces-libre/9782017189992).
            * Mini-projet : _logins réseau_
                * suivez le [guide](./Projets/mini_projet_logins/guide_eleve.md)
        * Pour vendredi :
            * Faire sur son cahier côté exercices, cet extrait du sujet NSI J1 2023 : [exo POO régions](./Exercices/Theme_Programmation/POO/exo_poo_met2023j1.pdf)

    ??? done "Séance 3 : vendredi 8/09/2023"

        * Séance du jour :    
            * 📅 Chapitre traité : [POO](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/POO/P1_Stucturer_avec_des_classes/P1_cours_POO/)    
            * Le point sur le vocabulaire de la POO, [QCM p. 41 du manuel](https://mesmanuels.fr/acces-libre/9782017189992). On s'aide de la [synthèse de cours](./T4_Programmation/POO/Synth%C3%A8se/synthese_poo.md).
            * Le point sur la syntaxe pour la POO en Python : faire cet exercice <https://e-nsi.gitlab.io/pratique/N0/700-syntaxe_poo/sujet/> ➡️  [correction](./Exercices/e-nsi/pour_demarrer/programmeur.saisit(code).py)
            * [TP POO](https://capytale2.ac-paris.fr/web/c/9f76-1759503)       
            * Mini-projet : _logins réseau_
                * suivez le [guide](./Projets/mini_projet_logins/guide_eleve.md)
        * Pour mardi :
            * Faire deux exercices dans [Capytale](https://capytale2.ac-paris.fr/web/c/6a56-1788958)
        * Pour vendredi prochain :
            * Interrogation écrite courte sur le chapitre POO



    ??? done "Séance 4 : mardi 12/09/2023"

        * Séance du jour : 
        
            * 📅 Chapitre traité : [POO](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/POO/P1_Stucturer_avec_des_classes/P1_cours_POO/)    
    
                * Correction d'un  exercice sur les dictionnaires dans [Capytale](https://capytale2.ac-paris.fr/web/c/6a56-1788958) ➡️  [correction en Python de dico_occurrences](./Exercices/e-nsi/a_maitriser/dico_occurrences.py)
                * Un exercice de  POO en Python : faire cet exercice <https://e-nsi.gitlab.io/pratique/N2/700-poo_chien/sujet/> ➡️  [correction](./Exercices/e-nsi/guides/chien_poo.py)
                * [TP POO](https://capytale2.ac-paris.fr/web/c/9f76-1759503) => on traite l'exercice 2  

            * 📅 Chapitre traité : [Modèle relationnel](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/P1_Mod%C3%A8le_relationnel.md)
                * On avance dans le document [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/ressources/TNSI-Cours-ModeleRelationnelV1-2022.pdf)

        * Pour jeudi :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Faire ces deux exercices sur [Capytale](https://capytale2.ac-paris.fr/web/c/2ebe-1807352)
        * Pour vendredi :
            * Interrogation écrite courte sur le chapitre POO

    

    ??? done "Séance 5 : jeudi 14/09/2023"
    
        * Séance du jour : 
        
            * 📋 Correction d'exercice   
    
                * Correction d'un  exercice sur les dictionnaires dans  [Capytale](https://capytale2.ac-paris.fr/web/c/2ebe-1807352)➡️  [correction en Python de dico_jaime](./Exercices/e-nsi/a_maitriser/dico_jaime.py)

            *  🏊 Entraînement sur la POO :
    
                * <https://e-nsi.gitlab.io/pratique/N2/700-poo_train/sujet/>   ➡️ [correction en Python de POO_Train](./Exercices/e-nsi/guides/poo_train.py)

            * 📅 Chapitre traité : [Modèle relationnel](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/P1_Mod%C3%A8le_relationnel.md)
                * On avance dans le document [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/ressources/TNSI-Cours-ModeleRelationnelV1-2022.pdf)

        * Pour mardi 19/09 :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Faire cet exercice sur [Capytale](https://capytale2.ac-paris.fr/web/c/4721-1823765)
        * Pour vendredi :
            * Interrogation écrite sur le chapitre POO


    ??? done "Séance 7 : vendredi 15/09/2023"
    
        * Séance du jour : 

            * 💯 Interrogation écrite sur le chapitre POO
        
            * 📅 Chapitre traité : [Modèle relationnel](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/P1_Mod%C3%A8le_relationnel.md)
                * On avance dans le document [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/ressources/TNSI-Cours-ModeleRelationnelV1-2022.pdf)

            *  🏊 Entraînement sur les algorithmes de première : Carnet Capytale : <https://capytale2.ac-paris.fr/web/c/c01d-1834688>
            *  [Correction soleil couchant](./Exercices/e-nsi/a_maitriser/soleil_couchant.py)
            *  [Correction Chiffre de Cesar](./Exercices/e-nsi/a_maitriser/chiffre_cesar.py)
            *  [Correction Tri par sélection](./Exercices/e-nsi/a_maitriser/tri_selection.py)
            *  [Correction 8 dames](./Exercices/e-nsi/a_maitriser/8dames.py)
    
        * Pour mardi 19/09 :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Finir les  exercices sur [Capytale](https://capytale2.ac-paris.fr/web/c/c01d-1834688)



    ??? done "Séance 7 : mardi 19/09/2023"
    
        * Séance du jour : 

            * 💯 Retour de l'interrogation écrite sur le chapitre POO
        
            * 📅 Chapitre traité : [Modèle relationnel](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/P1_Mod%C3%A8le_relationnel.md)
                * On termine le dernier exercice du document [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C1_Modele_Relationnel/ressources/TNSI-Cours-ModeleRelationnelV1-2022.pdf)

            *  🏊 Entraînement sur les algorithmes de première : Carnet Capytale : <https://capytale2.ac-paris.fr/web/c/c01d-1834688>
            *  [Correction soleil couchant](./Exercices/e-nsi/a_maitriser/soleil_couchant.py)
            *  [Correction Chiffre de Cesar](./Exercices/e-nsi/a_maitriser/chiffre_cesar.py)
            

            * 📅 Chapitre traité : [Langage SQL](./T3_Bases_de_donn%C3%A9es/C2_SQL/P1_SQL.md)
                * Avancée autonome dans les cours et les exercices jusqu'à l'exercice 4 [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/TNSI-Cours-SQLV1-2022.pdf)
    
        * Pour jeudi 20/09 :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Finir les deux derniers exercices sur [Capytale](https://capytale2.ac-paris.fr/web/c/c01d-1834688)
            * Faire les deux exercices d'arithmétique du [carnet Capytale](https://capytale2.ac-paris.fr/web/c/e2bf-1856932/mcer)



    ??? done "Séance 8 : jeudi 21/09/2023"
    
        * Séance du jour : 

            *  🏊 Entraînement sur les algorithmes de première : deux exercices d'arithmétique du [carnet Capytale](https://capytale2.ac-paris.fr/web/c/e2bf-1856932/mcer) :
    
                *  [Changement de base en binaire](./Exercices/e-nsi/guides/conversion_binaire.py)
                *  [Nombre de zéros à la fin d'un entier](./Exercices/e-nsi/a_maitriser/nb_zeros.py)
            

            * 📅 Chapitre traité : [Langage SQL](./T3_Bases_de_donn%C3%A9es/C2_SQL/P1_SQL.md)
    
                * Cours :
                    * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/7a2b-610499)
                    * Correction de l'exercice 4 [Cours + Exercices](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/TNSI-Cours-SQLV1-2022.pdf)
                    * valeurs calculées et alias de colonnes
                    * fonctions d'agrégation  `MIN`, `MAX`, `COUNT`, `AVG`, `SUM` : exercice 5
                    * Requêtes d'interrogation sur plusieurs tables, jointures : définition et exercice 6
                * TP1 :
                    * [matériel à télécharger](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/materiel-tp1-sql.zip)
                    *  [correction](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/correction-tp1.zip)
    
        * Pour vendredi 22/09 :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Faire les deux exercices sur les tableaux Python de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/ac3e-1874628)
        * Pour jeudi 27/09 :
            * [DM 1 non noté]()



    ??? done "Séance 9 : vendredi 22/09/2023"
    
        * Séance du jour : 

            *  🏊 Entraînement sur les algorithmes de première :  correction de deux exercices sur les tableaux Python de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/ac3e-1874628):
    
                *  [Ou exclusif sur deux listes](./Exercices/e-nsi/a_maitriser/ou_exclusif.py)
                *  [Tri d'un tableau de 0 et de 1](./Exercices/e-nsi/guides/zeros_et_uns.py)
            

            * 📅 Chapitre traité : [Langage SQL](./T3_Bases_de_donn%C3%A9es/C2_SQL/P1_SQL.md)
    
                
                * TP1 :
                    * [matériel à télécharger](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/materiel-tp1-sql.zip)
                    *  [correction](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/correction-tp1.zip)
            
    
        * Pour mardi  25/09 :
            * Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Faire les deux exercices sur les tris  de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/f480-1884493)
        * Pour jeudi 27/09 :
            * [DM 1 non noté](https://nuage03.apps.education.fr/index.php/s/ML3D4oSkoiFFWPW)



    ??? done "Séance 10 : mardi 26/09/2023"
    
        * Séance du jour : 

            *  🏊 Entraînement sur les algorithmes de première :  correction de deux exercices sur les tris  de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/f480-1884493):
    
                *  [tri par sélection](./Exercices/e-nsi/a_maitriser/tri_selection.py)
                *  [tri par insertion](./Exercices/e-nsi/guides/tri_insertion.py)
            

            * 📅 Chapitre traité : [Langage SQL](./T3_Bases_de_donn%C3%A9es/C2_SQL/P1_SQL.md)
    
                
                * TP1 :
                    * [matériel à télécharger](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/materiel-tp1-sql.zip)
                    *  [correction](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/correction-tp1.zip)
                *  Cours :
                    * Jointures multiples : exercice 7
                *  TP2 :
                    * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/b8d6-978075)
                    * [Archive avec base, énoncé, corrigé, schéma](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/tp2.zip)

    
        * Pour jeudi  28/09 :
            
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * [DM 1 non noté](https://nuage03.apps.education.fr/index.php/s/ML3D4oSkoiFFWPW)

        * Pour vendredi 6/10 :
            * DS sur les chapitres SGBD et SQL


    ??? done "Séance 11 : jeudi 28/09/2023"
    
        * Séance du jour : 

                     

            * 🏊  Récolte du [DM 1 non noté](https://nuage03.apps.education.fr/index.php/s/ML3D4oSkoiFFWPW)
            * 📅 Chapitre traité : [Langage SQL](./T3_Bases_de_donn%C3%A9es/C2_SQL/P1_SQL.md)
    
                *  Cours :
                    * Requêtes d'insertion, de mise à jour, de suppression et exercice 8
                *  TP2 :
                    * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/b8d6-978075)
                    * [Archive avec base, énoncé, corrigé, schéma](./T3_Bases_de_donn%C3%A9es/C2_SQL/ressources/tp2.zip)

            * 📅 Chapitre traité : [Récursivité](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

                * [Synthèse de cours](./T4_Programmation/R%C3%A9cursivit%C3%A9/Synth%C3%A8se/synthese_recursivite.md)
                * On traite la [partie 1](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

    
        * Pour vendredi  29/09:
            
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Exposé Nyle
           
        * Pour lundi  2/10:
            * Traiter sur feuille [cet exercice de bac sur SQL](./Exercices/bac/2023/sujet_0.a/23-zero_a-ex_1.pdf)
        * Pour vendredi 6/10 :
            * DS sur les chapitres SGBD et SQL


    ??? done "Séance 12 : vendredi 29/09/2023"
    
        * Séance du jour : 

            * 🗣️  exposé oral sur le thème *Le langage SQL*
            * 📅 Chapitre traité : [Récursivité](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

                * [Synthèse de cours](./T4_Programmation/R%C3%A9cursivit%C3%A9/Synth%C3%A8se/synthese_recursivite.md)
                * Cours : on finit  la [partie 1](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md) et on traite la [partie 2](./T4_Programmation/R%C3%A9cursivit%C3%A9/P2_Correction_et_terminaison/P2_correction_et_terminaison.md)
                * Exercices sur récursivité et chaines de caractères [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/641a-1944392)
                * [TP1](https://capytale2.ac-paris.fr/web/c/8ee0-1764536)

        * Pour lundi  2/10:
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            * Traiter sur feuille [cet exercice de bac sur SQL](./Exercices/bac/2023/sujet_0.a/23-zero_a-ex_1.pdf)
        * Pour vendredi 6/10 :
            * DS sur les chapitres SGBD et SQL


??? bug "Octobre"

    ??? done "Séance 13 : mardi 3/10/2023"
    
        * Séance du jour : 

            * Correction de [cet exercice de bac sur SQL](./Exercices/bac/2023/sujet_0.a/23-zero_a-ex_1.pdf). Lien vers un [corrigé](./Exercices/bac/2023/sujet_0.a/23-zero_a-ex_1-correction.pdf)
            * 📅 Chapitre traité : [Récursivité](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

                * [Synthèse de cours](./T4_Programmation/R%C3%A9cursivit%C3%A9/Synth%C3%A8se/synthese_recursivite.md)
                * Exercices sur récursivité et chaines de caractères [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/641a-1944392)
                * [TP1](https://capytale2.ac-paris.fr/web/c/8ee0-1764536)
                * [Fin du cours : avantages et défauts de la récursivité](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/R%C3%A9cursivit%C3%A9/P4_Avantages_et_d%C3%A9fauts/P4_Avantages_et_d%C3%A9fauts/)

        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
        * Pour jeudi 5/10 :
            * Faire au moins un exercice de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/1605-1973574)
            * Lire ce carnet [Algorithme de Horner et récursivité terminale](https://capytale2.ac-paris.fr/web/c/f25a-1986924)
        * Pour mardi 10/10 :
            * DS sur les chapitres SGBD et SQL


    ??? done "Séance 14 : jeudi 5/10/2023"
    
        * Séance du jour : 
            * 🏊 Correction de l'exercice Nombre de chemins dans une grille de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/1605-1973574) Lien vers un [corrigé](./Exercices/e-nsi/guides/nb_chemins_grilles.py)
            * 📅 Chapitre traité : [Récursivité](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

                * [Synthèse de cours](./T4_Programmation/R%C3%A9cursivit%C3%A9/Synth%C3%A8se/synthese_recursivite.md)
                * Retour sur ce carnet [Algorithme de Horner et récursivité terminale](https://capytale2.ac-paris.fr/web/c/f25a-1986924)                
                * [Fin du cours : avantages et défauts de la récursivité](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/R%C3%A9cursivit%C3%A9/P4_Avantages_et_d%C3%A9fauts/P4_Avantages_et_d%C3%A9fauts/)
                * [TP2](https://capytale2.ac-paris.fr/web/c/11e1-1763427)
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
        * Pour vendredi 6/10 :
            * Faire [cet exercice type bac](./Exercices/bac/2022/asie-j2/22-A2-ex5.pdf)
            * Lire ce carnet [Algorithme de Horner et récursivité terminale](https://capytale2.ac-paris.fr/web/c/f25a-1986924)
        * Pour mardi 10/10 :
            * DS sur les chapitres SGBD et SQL


    ??? done "Séance 15 : vendredi 6/10/2023"
    
        * Séance du jour : 
  
            * 🗣️  exposé oral sur le thème *Le langage C* 
            * 🏊 Correction de [l'exercice type bac](./Exercices/bac/2022/asie-j2/22-A2-ex5.pdf). Lien vers un [corrigé](./Exercices/bac/2022/asie-j2/22-A2-ex5-correction.pdf)
            * 📅 Chapitre traité : [Récursivité](./T4_Programmation/R%C3%A9cursivit%C3%A9/P1_Algorithmes_R%C3%A9cursifs_Fonctions_R%C3%A9cursives/P1_Algorithmes_r%C3%A9cursifs_fonctions_r%C3%A9cursives.md)

                * [Synthèse de cours](./T4_Programmation/R%C3%A9cursivit%C3%A9/Synth%C3%A8se/synthese_recursivite.md)                       
                * [Fin du cours : avantages et défauts de la récursivité](https://fjunier.forge.aeif.fr/terminale_nsi/T4_Programmation/R%C3%A9cursivit%C3%A9/P4_Avantages_et_d%C3%A9fauts/P4_Avantages_et_d%C3%A9fauts/)  : exercices 4 et 6

            * 📅 Chapitre traité : [Types Abstraits](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C1_Types_abstraits/P1_Type_Abstrait/P1_Type_Abstrait.md)
                * [Synthèse de cours](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C1_Types_abstraits/P2_Synth%C3%A8se/synthese_type_abstrait.md)
                * [Carnet Capytale avec les exemples du cours](https://capytale2.ac-paris.fr/web/c/0b0d-2009866)
   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
        * Pour mardi 10/10 :
            * DS sur les chapitres SGBD et SQL


    ??? done "Séance 16 : mardi 10/10/2023"
    
        * Séance du jour : 
  

            * 💯 DS sur les SGBD et le langage SQL
            * 🚧  On présente le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)
   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
        * Pour jeudi 12/10 :
            * Faire les deux premiers exercices  de  ce [carnet](https://capytale2.ac-paris.fr/web/c/78c8-2040971)  : fusion de listes triées et premiers exemples de programmation dynamique.


    ??? done "Séance 17 : jeudi 12/10/2023"

        * 🏊 Correction de l'exercice  fusion de listes triées  du [carnet](https://capytale2.ac-paris.fr/web/c/78c8-2040971) :
            * [Correction fusions listes triées](./Exercices/e-nsi/guides/fusion_liste_trie.py)
            * [Correction nombres de Schroder](./Exercices/e-nsi/guides/schroder.py)
            * [Correction nombres de Delannoy](./Exercices/e-nsi/guides/delannoy.py)
        * Séance du jour : 
  
            * 📅 Chapitre traité : [Types Abstraits](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C1_Types_abstraits/P1_Type_Abstrait/P1_Type_Abstrait.md)
                * [Synthèse de cours](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C1_Types_abstraits/P2_Synth%C3%A8se/synthese_type_abstrait.md)
                * [Carnet Capytale avec les exemples du cours](https://capytale2.ac-paris.fr/web/c/0b0d-2009866)
                * Aujourd'hui on présente le type abstrait *dictionnaire*
            *  📅 Chapitre traité : [Listes](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C2_Listes/P2_Liste/P2_Liste.md).
               *  Aujourd'hui on présente le type abstrait *liste* et on traite les exercices 3 et 4.
            * 🚧  On commence le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)
   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
        * Pour vendredi 13/10 :
            * Faire les exercices  de  ce [carnet](https://capytale2.ac-paris.fr/web/c/2793-2078282) 


    ??? done "Séance 18 : vendredi 13/10/2023"

        * 🏊 Correction des exercices  de ce [carnet](https://capytale2.ac-paris.fr/web/c/2793-2078282)  :
            * correction de [Fibonacci](./Exercices/e-nsi/guides/fibonacci.py)
            * correction de [ecretage](./Exercices/e-nsi/guides/ecretage.py)
            * correction de [anagrammes](./Exercices/e-nsi/aventure/anagrammes.py)
        * Séance du jour :   

            *  📅 Chapitre traité : [Listes](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C2_Listes/P2_Liste/P2_Liste.md).
               *  Aujourd'hui on termine l'exercice 4
               *  Exercice crayon en main sur l'utilisation du type abstrait liste :
                
                *  Représenter les listes lis1, lis2, lis3 et lis 4 après exécutions du programme suivant (on suppose que `inserer(lis, vam)` renvoie une copie de la liste avec val ajouté en tête ):

                ~~~python
                lis1 = creer_liste()
                lis2 = inserer(lis1, 8)
                lis3 = inserer(lis2, 4)
                lis4 = inserer(lis3, 7)
                ~~~

                * Valeur de `tete(queue(queue(lis4)))` ?
                * Comment accéder à l'élément 8 depuis `lis4` ? et à 7 ?
                * Ecrire une fonction de signature `somme(lis)` qui renvoie la somme des éléments d'une liste d'entiers `lis`
                * Ecrire une fonction de signature `maximum(lis)` qui renvoie le maixmum d'une liste d'entiers `lis`
                   

            *  📅 Chapitre traité : [Listes chaînées](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C2_Listes/P3_Listes_Chainees/P3_Liste_Chainees.md).
                  *  Aujourd'hui on présente l'implémentation des listes par chainage de cellules implémentées par des tuples ou par des objets à champs. 

   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)  
        * Pour mardi  17/10 :
            * Faire sur feuille cet [exercice type Bac](./Exercices/bac/2023/sujet_0.b/23-zero_b-ex_2.pdf)
            * Faire les exercices  de  ce [carnet](https://capytale2.ac-paris.fr/web/c/21c1-2078396)
        * Pour vendredi 20/10 :
            * DS sur la récursivité. 



    ??? done "Séance 18 : mardi 17/10/2023"

        * 🏊 Correction des exercices  de ce [carnet](https://capytale2.ac-paris.fr/web/c/21c1-2078396)  :
            * [Correction train POO](./Exercices/e-nsi/guides/poo_train.py)
        * 🏊  Correction de cet [exercice sur feuille](./Exercices/bac/2023/sujet_0.b/23-zero_b-ex_2.pdf), lien vers un [corrigé](./Exercices/bac/2023/sujet_0.b/23-zero_b-ex_2_correc.pdf)
        * Séance du jour :  

            *  📅 Chapitre traité : [Listes chaînées](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C2_Listes/P3_Listes_Chainees/P3_Liste_Chainees.md).
                  *  Aujourd'hui on termine la présentation l'implémentation des listes par chainage de cellules implémentées par des tuples ou par des objets à champs : exercices 5 et 6. [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/ec74-1763635)

            *  📅 Chapitre traité : [Listes chaînées mutables](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C2_Listes/P4_Listes_Chainees_mutables/P4_Liste_Chainees_mutables/)
                  *  Distinction entre objets *mutables* et *immuables*, exemples des types natifs de Python et cas des implémentations du type Liste
                  *  On fait l'exercice 6
  
            *  🚧  On continue le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)

   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)  
        * Pour jeudi  19/10 :
            *  DS sur la récursivité.
            * Faire l'exercice  de  ce [carnet](https://capytale2.ac-paris.fr/web/c/73aa-2105177)


    ??? done "Séance 19 : jeudi 19/10/2023"

        
        * 🏊 Correction des exercices  de ce [carnet](https://capytale2.ac-paris.fr/web/c/73aa-2105177).
        * 💯 DS sur la récursivité
        * Séance du jour :  
            *  📅 Chapitre traité : [Listes chaînées mutables](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C2_Listes/P4_Listes_Chainees_mutables/P4_Liste_Chainees_mutables/)
                  *  Distinction entre objets *mutables* et *immuables*, exemples des types natifs de Python et cas des implémentations du type Liste
                  *  On fait l'exercice 6
  
            *  🚧  On continue le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md) : correction des  exercices 4 et 5, puis formation de quatre groupes : 

   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Exercices 


    ??? done "Séance 20 : vendredi 20/10/2023"

        
        * Séance du jour :  
            *  📅 Chapitre traité : [Listes chaînées mutables](./T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C2_Listes/P4_Listes_Chainees_mutables/P4_Liste_Chainees_mutables/)
                  *  Distinction entre objets *mutables* et *immuables*, exemples des types natifs de Python et cas des implémentations du type Liste
                  *  On fait l'exercice 7 du cours
                  * [TP](https://capytale2.ac-paris.fr/web/c/4870-1751321) => Exercice 1
  
            *  🚧  On continue le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)   : correction des  exercices 4 et 5, puis formation de quatre groupes : 
                  * groupe 1 : exercice 6 niveau  💪🏾 
                  * groupe 2 :  exercice 7 niveau 💪🏾
                  * groupe 3 : exercice 8 niveau 💪🏾💪🏾
                  * groupe 4 : exercice 9 niveau 💪🏾💪🏾💪🏾

   
        * Pour les prochains cours :
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour le vendredi de la rentrée résoudre l'exercice du projet correspondant à son groupe. Pour le vendredi suivant, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
               *  Pour chaque élève : 
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point


??? bug "Novembre"

    ??? done "Séance 21 : mardi 7/11/2023"     
        * Séance du jour : 
   
            * 💯 Retour du DS sur la récursivité avec son corrigé (voir dépot sur Moodle)
            *  📅 Chapitre traité : [Piles](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
  
                *  [Découverte et manipulation de l'interface](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
                *  [Différentes implémentations](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C3_Piles/P2_Impl%C3%A9mentations/P2_Impl%C3%A9mentations/):
                     *  Implémentation par un tableau dynamique Python
                     *  Implémentation par un tableau de taille fixée
              
       
  
            *  🚧  Travail sur le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)   : correction des  exercices 4 et 5, puis formation de quatre groupes : 
  
                  * groupe 1 : exercice 6 niveau  💪🏾 
                  * groupe 2 :  exercice 7 niveau 💪🏾
                  * groupe 3 : exercice 8 niveau 💪🏾💪🏾
                  * groupe 4 : exercice 9 niveau 💪🏾💪🏾💪🏾

   
        * Pour les prochains cours :
            * Pour jeudi : faire les exercices sur ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/f3bc-2207857)
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour  vendredi 10/11 résoudre l'exercice du projet correspondant à son groupe. Pour  vendredi 17/11suivant, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
  
               *  Pour chaque élève : 
  
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
       



    ??? done "Séance 21 : jeudi 9/11/2023"     
        * Séance du jour : 
   
            * Correction des exercices sur ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/f3bc-2207857):
                * [Carnet avec les corrections](https://capytale2.ac-paris.fr/web/c/0727-2215728)
            *  📅 Chapitre traité : [Piles](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
  
                *  [Découverte et manipulation de l'interface](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
                *  [Différentes implémentations](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C3_Piles/P2_Impl%C3%A9mentations/P2_Impl%C3%A9mentations/):
                     *  Implémentation par une liste chaînée
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/e39b-1764587)
              
       
  
            *  🚧  Travail sur le [projet seam carving](./Projets/P2_seam_carving/seam_carving.md)   : correction des  exercices 4 et 5, puis formation de quatre groupes : 
  
                  * groupe 1 : exercice 6 niveau  💪🏾 
                  * groupe 2 :  exercice 7 niveau 💪🏾
                  * groupe 3 : exercice 8 niveau 💪🏾💪🏾
                  * groupe 4 : exercice 9 niveau 💪🏾💪🏾💪🏾

   
        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour  vendredi 10/11 résoudre l'exercice du projet correspondant à son groupe. Pour  le vendredi 17/11, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
  
               *  Pour chaque élève : 
  
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
            *  Pour mardi 14/11 : préparer sur feuille cet [exercice de bac sur le type abstrait Pile](./Exercices/bac/2022/metropole-j2/metropole-j2-2022-poussette.pdf)



    ??? done "Séance 22 : vendredi/10/2023"     
        * Séance du jour : 
   
    
            *  🗣️  Oral exposé
            *  📅 Chapitre traité : [Piles](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
  
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/e39b-1764587)

            
            * 🏆 On commence le concours [Prologin](https://prologin.org/) => [carnet Capytale](https://capytale2.ac-paris.fr/web/c/a362-2230520) avec des squelettes de code pour récupérer les entrées des quatre premiers exercices.
   
        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour  le vendredi 17/11, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
  
               *  Pour chaque élève : 
  
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
            *  Pour mardi 14/11 : préparer sur feuille cet [exercice de bac sur le type abstrait Pile](./Exercices/bac/2022/metropole-j2/metropole-j2-2022-poussette.pdf)

    
    ??? done "Séance 23 : mardi 13/11/2023"     
        * Séance du jour : 
   
            *  📅 Chapitre traité : [Piles](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md)
  
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/b95b-2242715) ➡️  Exercice 5
                * Correction de  cet [exercice de bac sur le type abstrait Pile](./Exercices/bac/2022/metropole-j2/22-ME2-poussette-correction.md)

            *  📅 Chapitre traité : [Files](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C4_Files/P1_Interface/P1_Interface/)
                * Interface du type abstrait File
                * Implémentations par : liste chaînée, tableau circulaire, deux piles, `deque` 

        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour jeudi 16/11 : exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/6b7c-2262130)
            *  Pour  le vendredi 17/11, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
  
               *  Pour chaque élève : 
  
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
            *  Pour jeudi 23/11 : DS sur Listes et Piles.


    ??? done "Séance 24 : jeudi 16/11/2023"     
        * Séance du jour : 

            * 🏊 Correction des exercices  de ce [carnet](ttps://capytale2.ac-paris.fr/web/c/6b7c-2262130)  :
                * Correction dans ce [carnet](https://capytale2.ac-paris.fr/web/c/a3d1-2284473)
       
            *  📅 Chapitre traité : [Files](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C4_Files/P1_Interface/P1_Interface/)
                * Interface du type abstrait File
                * Implémentations par :  deux piles, `deque` 
                * [TP Files](https://capytale2.ac-paris.fr/web/c/0303-1764832)

        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour jeudi 16/11 : exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/6b7c-2262130)
            *  Pour  le vendredi 17/11, préparer une présentation orale (3 minutes par élève maximum) de l'algorithme et du code produits :
  
               *  Pour chaque élève : 
  
                  *  Qualité des explications sur 3 points
                  *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                  *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
            *  Pour jeudi 23/11 : DS sur Listes et Piles.
            *  Pour mardi 21/11 : traiter sur son cahier cet [exercice sur les piles](./Exercices/bac/2022/metropole-j1/22-ME1-ex1%20-Piles.pdf) + faire l'exercice du [TP1 Piles](https://capytale2.ac-paris.fr/web/c/b95b-2242715) => suivre ce lien où l'énoncé a été rectifiée



    ??? done "Séance 25 : vendredi 17/11/2023"     
        * Séance du jour : 

            * 🗣️  Évaluation orale du mini-projet Seam Carving  ([correction du projet sur Capytale](https://capytale2.ac-paris.fr/web/c/288e-2024303))
                 *  Pour chaque élève : 
    
                    *  Qualité des explications sur 3 points
                    *  Qualité de l'oral (fluidité, pas d'hésitations, audible) sur 2 points
                    *  Qualité de la posture (regarde l'auditoire, visage et corps mobiles) sur 1 point
            * 📅 Chapitre traité : [Piles](./T1_Structures_de_donn%C3%A9es/1_Structures_linéaires/C3_Piles/P1_Interface/P1_Interface.md) :
                * Deux exercices :  comparaison de de piles et tri par paquet d'une pile de notes dans [carnet Capytale](https://capytale2.ac-paris.fr/web/c/3adc-2297526) 
            *  📅 Chapitre traité : [Files](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/1_Structures_lin%C3%A9aires/C4_Files/P1_Interface/P1_Interface/)
                * [TP Files](https://capytale2.ac-paris.fr/web/c/0303-1764832)

        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour jeudi 23/11 : DS sur Listes et Piles.
            *  Pour mardi 21/11 : traiter sur son cahier cet [exercice sur les piles](./Exercices/bac/2022/metropole-j1/22-ME1-ex1%20-Piles.pdf) + faire l'exercice 6 du [TP1 Piles](https://capytale2.ac-paris.fr/web/c/b95b-2242715) => suivre ce lien où l'énoncé a été rectifiée 


    ??? done "Séance 26 : mardi 21/11/2023"     
        * Séance du jour : 

            *  🏊 Correction d'exercices sur les piles:
                  *  exercice 6 du [TP1 Piles](https://capytale2.ac-paris.fr/web/c/b95b-2242715)
                  *  [exercice écrit](./Exercices/bac/2022/metropole-j1/22-ME1-ex1%20-Piles.pdf)

            *  📅 Chapitre traité : [Protocoles de routage](./T3_Architecture_Système_Réseaux/C1_Routage/P1_Routage.md)
                * [Cours avec exercices](https://nuage03.apps.education.fr/index.php/s/FSSjMoEskxJx97X)
                * [Correction du cours](https://nuage03.apps.education.fr/index.php/s/Stydn7F5iiiYsAX)
                * Objectifs du jour : 
                    * révisions sur la notion de réseau informatique (hôtes, architecture client serveur)  , l'adressage des interfaces réseaux (MAC et IP), le modèle en couches des protocoles réseaux, DNS et protocoles d'application HTTP
      

        * Pour les prochains cours :
  
            *  Choisir son sujet d'exposé [ici](./T0_Histoire/C1_exposés/P1_exposés/exposés.md), le planning est [là](https://nuage03.apps.education.fr/index.php/s/MJWoNXxrdfdjcmi)
            *  Pour jeudi 23/11 : DS sur Listes et Piles.
            *  Pour vendredi 24/11 :   faire les exercices dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/b691-2325535)


    ??? done "Séance 27 : jeudi 23/11/2023"     
        * Séance du jour : 

            * 💯 DS de 2 heures sur les types Liste et Pile


    ??? done "Séance 28 : vendredi 24/11/2023"     
        * Séance du jour : 

        *  🏊 Correction d'exercices sur les dictionnaires dans ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/b691-2325535)


        *  📅 Chapitre traité : [Protocoles de routage](./T3_Architecture_Système_Réseaux/C1_Routage/P1_Routage.md)
            *  Le point sur le modèle en couches d'Internet : [Affiche réalisée par Nicolas Labat](./T3_Architecture_Système_Réseaux/C1_Routage/ressources/Model_internet_A1.pdf)
            * [Cours avec exercices](https://nuage03.apps.education.fr/index.php/s/FSSjMoEskxJx97X)
            * [Correction du cours](https://nuage03.apps.education.fr/index.php/s/Stydn7F5iiiYsAX)
            * Objectifs du jour : 
                * notion de routage (Objectif 3, pages 9 et 10 du cours) : 
                    * 🛠️  Exercice 3 et 4 (le 4 uniquement pour les plus rapides)
                * un premier protocole de routage dynamique à vecteur de distance : RIP 
                    * 🛠️  Exercices 5 et 6
                * un second  protocole de routage dynamique à vecteur de liens : OSPF
                    * 🛠️  Exercices 7 et 8

        * Pour les prochains cours :
  
            *  Pour vendredi 1/12 :  traiter  sur copie double en tant que DM 2 cet [exercice sur le routage](./Devoirs/DM/2023-2024/DM2-2024.pdf) extrait du [sujet 0A du bac NSI 2024](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g#summary-item-16).
            *  Pour mardi 28/11 : [exercices de ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ff05-2365179)


    ??? done "Séance 29 : mardi 28/11/2023"     
        * Séance du jour : 

        *  🏊 Correction d'exercices de  ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/ff05-2365179)


        *  📅 Chapitre traité : [Protocoles de routage](./T3_Architecture_Système_Réseaux/C1_Routage/P1_Routage.md)
            *  Correction de l'exercice 8.

        *  📅 Chapitre traité : [Diviser Pour Régner](./T5_Algo/1_Méthodes/C1_Diviser_pour_régner/P1_Recherche_dichotomique/P1_Recherche_dichotomique.md)
            * Objectifs du jour : 
              * Recherche dichotomique itérative et récursive (implémentation, preuve de terminaison et preuve de correction, preuve de complexité dans le pire des cas)
              * Définition de la méthode Diviser Pour Régner
           


        * Pour les prochains cours :
  
            *  Pour vendredi 1/12 :  traiter  sur copie double en tant que DM 2 cet [exercice sur le routage](./Devoirs/DM/2023-2024/DM2-2024.pdf) extrait du [sujet 0A du bac NSI 2024](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g#summary-item-16).


??? bug "Décembre"

    ??? done "Séance 30 : vendredi 1/12/2023"     
        * Séance du jour : 

            *  🏊 Correction d'exercices de  ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/ff05-2365179)  : exercice 2 sur la recherche du point d'insertion par dichotomie


            *  📓 Récolte du DM 2 sur cet [exercice sur le routage](./Devoirs/DM/2023-2024/DM2-2024.pdf)

            *  📅 Chapitre traité : [Diviser Pour Régner](./T5_Algo/1_Méthodes/C1_Diviser_pour_régner/P1_Recherche_dichotomique/P1_Recherche_dichotomique.md)
                * Objectifs du jour : 
                  * Tri fusion : [Exercices 4, 5, 6, 7](https://fjunier.forge.aeif.fr/terminale_nsi/T5_Algo/1_M%C3%A9thodes/C1_Diviser_pour_r%C3%A9gner/P2_Tri_fusion/P3_Tri_fusion/)
            *  [Synthèse du cours DPR](https://fjunier.forge.aeif.fr/terminale_nsi/T5_Algo/1_M%C3%A9thodes/C1_Diviser_pour_r%C3%A9gner/Synth%C3%A8se/synthese_dpr/)
            *  [TP 1](https://capytale2.ac-paris.fr/web/c/edad-1932331)
           


        * Pour les prochains cours :
  
            *  Pour mardi 5/12 : faire les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ca3b-2417975)
            *  Pour vendredi 8/12 : [DM 3](./Devoirs/DM/2023-2024/DM3-2024.pdf)
            
 
    ??? done "Séance 31 : mardi  5/12/2023"     
        * Séance du jour : 

            * 🗣️  exposé oral 
  
            *  🏊 Correction d'exercices de  [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ca3b-2417975)  :  [carnet avec les corrections](https://capytale2.ac-paris.fr/web/c/afa0-2465130)

            *  📓 Retour du DM 2 sur cet [exercice sur le routage](./Devoirs/DM/2023-2024/DM2-2024.pdf) avec son [corrigé](./Devoirs/DM/2023-2024/correction-dm2.md)

            *  📅 Chapitre traité : [Diviser Pour Régner](./T5_Algo/1_Méthodes/C1_Diviser_pour_régner/P1_Recherche_dichotomique/P1_Recherche_dichotomique.md)
                * Objectifs du jour : 
                  * [Exercice 2](https://fjunier.forge.aeif.fr/terminale_nsi/T5_Algo/1_M%C3%A9thodes/C1_Diviser_pour_r%C3%A9gner/P1_Recherche_dichotomique/P1_Recherche_dichotomique/#retour-sur-la-recherche-dichotomique)  => problème de la recherche du maximum
                  * Tri fusion : [Exercices 5, 6, 7, 8](https://fjunier.forge.aeif.fr/terminale_nsi/T5_Algo/1_M%C3%A9thodes/C1_Diviser_pour_r%C3%A9gner/P2_Tri_fusion/P3_Tri_fusion/).  On travaillera dans ce [carnet](https://capytale2.ac-paris.fr/web/c/3576-2464790) où quelques coquilles ont été corrigées.
                *  [Synthèse du cours DPR](https://fjunier.forge.aeif.fr/terminale_nsi/T5_Algo/1_M%C3%A9thodes/C1_Diviser_pour_r%C3%A9gner/Synth%C3%A8se/synthese_dpr/)
                *  [TP 1](https://capytale2.ac-paris.fr/web/c/3007-1932279)  : exercice 2 rotation d'une image
           


        * Pour les prochains cours :
  
            *  Pour jeudi  7/12 : exercice 1 du  [TP 1](https://capytale2.ac-paris.fr/web/c/edad-1932331)  : plus long préfixe commun d'un tableau de chaînes de  caractères avec un algorithme DPR
            *  Pour vendredi 8/12 : [DM 3](./Devoirs/DM/2023-2024/DM3-2024.pdf)
            *  Pour jeudi 14/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner


           
    ??? done "Séance 32 : jeudi 7/12/2023"     
        * Séance du jour : 

            * 🗣️  exposé oral 
            *  📓 Retour du DM 2 sur cet [exercice sur le routage](./Devoirs/DM/2023-2024/DM2-2024.pdf) avec son [corrigé](./Devoirs/DM/2023-2024/correction-dm2.md)

            *  📅 Chapitre traité : [Diviser Pour Régner](./T5_Algo/1_Méthodes/C1_Diviser_pour_régner/P1_Recherche_dichotomique/P1_Recherche_dichotomique.md)
               *  [TP 1](https://capytale2.ac-paris.fr/web/c/3007-1932279)  : exercice 1 plus long préfixe commun
               *  [💻 TP 2 sur Capytale](https://capytale2.ac-paris.fr/web/c/edad-1932331) : sous-tableau de somme maximale et tri rapide
            *  Constitution des groupes pour le mini-projet Noël 2023, découverte des sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


        * Pour les prochains cours :
  
            *  Pour vendredi 8/12 : [DM 3](./Devoirs/DM/2023-2024/DM3-2024.pdf)
            *  Pour mardi  19/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner


    ??? done "Séance 33 : vendredi 8/12/2023"     
        * Séance du jour : 

            * 🗣️  exposé oral 
            *  📓 Récolte  du [DM 3](./Devoirs/DM/2023-2024/DM3-2024.pdf)

            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)
                  *  Présentation des structures arborescentes
                  *  Définitions des arbres binaires et vocabulaires
                  *  Mesures d'un arbre binaire : taille et hauteur, inégalités
                  *  Exercices 1, 2 et 3
                  *  [Exercice d'appplication](./Exercices/arbres_binaires/exo_arbre1/exo_arbre1_2023.pdf)
            *  Constitution des groupes pour le mini-projet Noël 2023, découverte des sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


        * Pour les prochains cours :
  
            *  Pour mardi  12/12 : Faire les deux exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/ca3b-2417975)
            *  Pour mardi  19/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner


    ??? done "Séance 34 : mardi 12/12/2023"

        * Séance du jour : 

            * 🗣️  exposé oral 
            * Correction des deux exercices de ce [carnet Capytale](https://capytale2.ac-paris.fr/web/c/eb5b-2530060) :  [correction ici](https://capytale2.ac-paris.fr/web/c/626e-2536148)
            * A propos de la [Dichotomie](./Bac/C0_Algorithmes_de_référence/Dichotomie.md)
            *  📓 Retour  du [DM 3](./Devoirs/DM/2023-2024/DM3-2024.pdf) : [correction](./Devoirs/DM/2023-2024/correction-dm3.pdf)

            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)
                  *  Mesures d'un arbre binaire : taille et hauteur, inégalités
                  *  [Exercice d'appplication](./Exercices/arbres_binaires/exo_arbre1/exo_arbre1_2023.pdf)
                  *  Implémentation d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P3_Interface_et_impl%C3%A9mentations/P3_Interface_et_impl%C3%A9mentations/):
                    *  arbre binaire immuable avec une classe Noeud et un interface fonctionnelle
                    *  arbre binaire mutable avec une classe Noeud et une classe AB
            *  Constitution des groupes pour le mini-projet Noël 2023, découverte des sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


        * Pour les prochains cours :
  
            *  Pour jeudi 14/12 : Faire les exercices de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ecab-2536133)
            *  Pour mardi  19/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner




    ??? done "Séance 35 : jeudi 14/12/2023"

        * Séance du jour : 
  
            * 🏊  : [rituel du jour](https://frederic-junier.org/TNSI2023/rituels/20231214/rituel.html) 
            * 📰   Actualités  :
                * Un [memo sur la dichotomie](./Bac/C0_Algorithmes_de_référence/Dichotomie.md)
                * Une [page sur les épreuves terminales](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            *  🗣️  exposé oral 
            * Correction des deux exercices de ce [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ecab-2536133) =>  [correction ici](https://capytale2.ac-paris.fr/web/c/4eab-2555788)



            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)
                  *  Implémentation d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P3_Interface_et_impl%C3%A9mentations/P3_Interface_et_impl%C3%A9mentations/):
                    *  arbre binaire immuable avec une classe Noeud et un interface fonctionnelle : exercices 4 et 5
                    *  arbre binaire mutable avec une classe Noeud et une classe AB : exercice 6
                  
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


        * Pour les prochains cours :
  
            *  Pour vendredi 15/12 : Faire l'exercice de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/8bf4-2555896)
            *  Pour jeudi 22/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner



    ??? done "Séance 36 : vendredi 15/12/2023"

        * Séance du jour : 
  
            * 🏊  : [rituel du jour](https://frederic-junier.org/TNSI2023/rituels/20231215/rituel.html) 
            * 📰   Actualités  :
                * Un [memo sur la dichotomie](./Bac/C0_Algorithmes_de_référence/Dichotomie.md)
                * Une [page sur les épreuves terminales](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)

            * Correction de l'exercice de [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/8bf4-2555896) sur l'implémentation d'une classe d'Arbre Binaire =>  [correction ici](https://capytale2.ac-paris.fr/web/c/4eab-2555788)
         
              


            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)
               *  Parcours en profondeur d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P4_Parcours/P4_Parcours/) jusqu'à l'exercice 7
              
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


        * Pour les prochains cours :
  
            *  Pour jeudi 22/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner



    ??? done "Séance 37 : mardi 19/12/2023"

         * Séance du jour :

            * 🏊  : [rituel du jour](https://frederic-junier.org/TNSI2023/rituels/20231219/rituel.html) 
            * 📰   Actualités  :
  
                 * Un [memo sur la dichotomie](./Bac/C0_Algorithmes_de_référence/Dichotomie.md)
                 * Une [page sur les épreuves terminales](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)

            * Correction des deux exercices de ce [ce carnet Capytale](https://capytale2.ac-paris.fr/web/c/ecab-2536133) =>  [correction ici](https://capytale2.ac-paris.fr/web/c/4eab-2555788)
            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)* 
                *  Parcours en profondeur d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P4_Parcours/P4_Parcours/) exercice 8
                *  Parcours en largeur d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P4_Parcours/P4_Parcours/) : exercice 9
                * [TP 1 sur arbres binaires](https://capytale2.ac-paris.fr/web/c/b10d-1765274)
  
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)


         * Pour les prochains cours :
 
             *  Pour jeudi 22/12 : DS sur les chapitres Files, Routage et Diviser Pour Régner


    ??? done "Séance 38 : jeudi 21/12/2023"

         * 💯 DS de 2 h sur les thèmes File, Routage et Diviser Pour Régner


    ??? done "Séance 39 : vendredi 22/12/2023"

         * Séance du jour :

            * 🏊  : [rituel du jour](https://frederic-junier.org/TNSI2023/rituels/20231221/rituel.html) 
            * 📰   Actualités  :
                 * Une [page sur les épreuves terminales](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                 * Mise à jour de la page [Algorithmes de référence](./Bac/C0_Algorithmes_de_référence/C0_Algorithmes_de_référence.md)

            *  📅 Chapitre traité : [Structures arborescentes et Arbres binaires](./T1_Structures_de_données/2_Structures_arborescentes/C0_Structure_arborescente/P0_Structure_arborescente.md)* 
                *  Parcours en largeur d'un arbre binaire [Cours](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/2_Structures_arborescentes/C1_Arbre_binaire/P4_Parcours/P4_Parcours/) : exercice 9
                * [TP 1 sur arbres binaires](https://capytale2.ac-paris.fr/web/c/b10d-1765274)
  
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)

??? bug "Janvier"



    ??? done "Séance 40 : mardi 09/01/2024"

        * Séance du jour :

            * 🏊  : [rituel du jour](https://capytale2.ac-paris.fr/web/c/3fc4-2677326) => parcours postfixe, infixe,  préfixe ou en largeur  d'un arbre binaire
            *  💯 Retour des copies du DS de 2 h sur les thèmes File, Routage et Diviser Pour Régner et son corrigé
            *  📅 Chapitre traité : [Algorithmes gloutons](./T5_Algo/1_Méthodes/C2_Algorithmes_gloutons/P1_Voyageur_de_commerce/P1_Voyageur_de_commerce.md)
                * Problèmes du voyageur de commerce et du rendu de monnaie.
                * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/f80d-2658451)
  
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)
        *  Travail à faire :
           *   DS d'1 heure sur arbres binaires le vendredi 19/01
           *  Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
           *  Bac Blanc de 3 h 30 le samedi 3/02


    ??? done "Séance 41 : jeudi 11/01/2024"

        * Séance du jour :
  
            *  🗣️  exposé oral : _De la machine de Turing aux premiers ordinateurs_ 
            *  📰   Actualités  :
               *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque)
            *  📅 Chapitre traité : [Algorithmes gloutons](./T5_Algo/1_Méthodes/C2_Algorithmes_gloutons/P1_Voyageur_de_commerce/P1_Voyageur_de_commerce.md)
                * Problème du rendu de monnaie.
                * [Carnet Capytale](https://capytale2.ac-paris.fr/web/c/f80d-2658451)
  
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)

            *  📅 Chapitre traité : [Processus](./T3_Archi_Sys_Réseau/C2_Processus/P1_Processus.md)
               *  [Cours avec exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/TNSI-Cours-Processus-2022.pdf)
               *  [Correction des exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/Correction-TNSI-Cours-Processus-2022.pdf)
  
        * Travail à faire :  
  
            * DS d'1 heure sur arbres binaires, algorithmes gloutons et processus le vendredi 19/01
            * Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
            * Bac Blanc de 3 h 30 le samedi 3/02


    ??? done "Séance 42 : vendredi 12/01/2024"

        * Séance du jour :
  
            *  [Exercice spécial](https://capytale2.ac-paris.fr/web/c/70f1-2716557)
            *  📰   Actualités  :
               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque)
          
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)

            *  📅 Chapitre traité : [Processus](./T3_Archi_Sys_Réseau/C2_Processus/P1_Processus.md)
               
                  *  [Cours avec exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/TNSI-Cours-Processus-2022.pdf) :
                      * Définition d'un processus
                      * Multiprogrammation 
                      * Cycle de vie d'un processus : exercice 3
                      * Ordonnancement : définition et présentation d'algorithmes
                  *  [Correction des exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/Correction-TNSI-Cours-Processus-2022.pdf)
  
        * Travail à faire :  
  
            * DS d'1 heure sur arbres binaires, algorithmes gloutons et processus le vendredi 19/01
            * Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
            * Bac Blanc de 3 h 30 le samedi 3/02
  

    ??? done "Séance 43 : mardi 16/01/2024"

        * 💯 DS de 1 h sur le thème Arbre binaires
        * Séance du jour :  
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe été créée, les codes personnels ont été envoyés par la messagerie         
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)
            *  📅 Chapitre traité : [Processus](./T3_Archi_Sys_Réseau/C2_Processus/P1_Processus.md)               
                  *  [Cours avec exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/TNSI-Cours-Processus-2022.pdf) :
                      * Ordonnancement : exercices 4, 5 et 6
                      * Ressource en accès exclusif et verrou : exercices 8 et 9
                      * Interblocage : exercices 10 et 11 et définition 
                  *  [Correction des exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/Correction-TNSI-Cours-Processus-2022.pdf)  
        * Travail à faire :    
            * DS d'1 heure sur arbres binaires le vendredi 19/01
            * Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
            * Bac Blanc de 3 h 30 le samedi 3/02
  


    ??? done "Séance 44 : jeudi 18/01/2024"

        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  🪧  Présentation de la spé NSI en première NSI          
            *  Travail sur le mini-projet Noël 2023, sujets dans [Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094)
            *  📅 Chapitre traité : [Processus](./T3_Archi_Sys_Réseau/C2_Processus/P1_Processus.md)               
                  *  [Cours avec exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/TNSI-Cours-Processus-2022.pdf) :
                      * Interblocage : exercices 10 et 11 et définition 
                  *  [Correction des exercices](./T3_Archi_Sys_Réseau/C2_Processus/ressources/Correction-TNSI-Cours-Processus-2022.pdf)  
        * Travail à faire : 
            * Pour mardi résoudre à la maison au moins 5 exercices d'entrainement à l'épreuve pratique sur Code Puzzle   
            * DS d'1 heure sur arbres binaires, algorithmes gloutons et processus le vendredi 19/01
            * Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
            * Bac Blanc de 3 h 30 le samedi 3/02


    ??? done "Séance 45 : vendredi 19/01/2024"

        * 💯 DS de 1 h sur le thème Arbre binaires
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  🪧  Présentation de la spé NSI en première NSI          

            *  📅 Chapitre traité :      Arbres binaires de recherche          
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Définitions et exercice 1
        * Travail à faire : 
            * Pour mardi résoudre à la maison au moins 5 exercices d'entrainement à l'épreuve pratique sur Code Puzzle   
            * Rendu du code du  mini-projet directement dans Capytale ou dans une archive envoyée par mail le mardi 23/01
            * Bac Blanc de 3 h 30 le samedi 3/02


    ??? done "Séance 46 : mardi 23/01/2024"

        * 💯 Retour du DS de 1 h sur le thème Arbre binaires : [sujet](https://nuage03.apps.education.fr/index.php/s/SpaGaLBPGZGCDiP) et [corrigé](https://nuage03.apps.education.fr/index.php/s/f7TS7R4qHM4iwHr)
        * 🏊  : [Exercice sur l'adressage IP et les masques de sous-réseau](https://nuage03.apps.education.fr/index.php/s/bJYznqLSJHom857) => à faire en mode déconnecté.  
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  🪧  Présentation de la spé NSI en première NSI          

            *  📅 Chapitre traité :    Arbres binaires de recherche  (ABR)       
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Définitions et exercice 1
                      * Propriétés d'un ABR (maximum, minimum, parcours infixe et tri) : exercices 2 et 3
                      * Implémentations : arbre binaire de recherche immuable (fonctionnel) ou mutable (POO), exercices 3 et 4 => [Carnet Capytale de saisie des codes](https://capytale2.ac-paris.fr/web/c/b1e8-2804836)
                      * Recherche dans un ABR : exercice 5
        * Travail à faire : 
            * Pour vendredi, faire les exercices 1 et 3 du sujet de [Métropole J2 juin 2021](https://e-nsi.forge.aeif.fr/ecrit/2021/metropole-juin-j2/spe-numerique-informatique-2021-metro-cand-libre-2-sujet-officiel.pdf) 
            * Bac Blanc de 3 h 30 le samedi 3/02


    ??? done "Séance 47 : vendredi 25/01/2024"

        * 🏊  : [Rituel du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/e4c5-2818520) : tris par sélection, insertion et manipulation de listes Python
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  🪧  Présentation de la spé NSI en première NSI          
            * 🏊  : correction des exercices 1 et 3 du sujet de [Métropole J2 juin 2021](https://e-nsi.forge.aeif.fr/ecrit/2021/metropole-juin-j2/spe-numerique-informatique-2021-metro-cand-libre-2-sujet-officiel.pdf) 
            *  📅 Chapitre traité :    Arbres binaires de recherche  (ABR)       
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Implémentations : arbre binaire de recherche immuable (fonctionnel) ou mutable (POO), exercices 3 et 4 => [Carnet Capytale de saisie des codes](https://capytale2.ac-paris.fr/web/c/b1e8-2804836)
                      * Recherche dans un ABR : exercice 6
                      * Ajout dans un ABR : exercices 7 et 8
        * Travail à faire : 
            * Bac Blanc de 3 h 30 le samedi 3/02
 

??? bug "Février"


    ??? done "Séance 48 : mardi 06/02/2024"
        * 💯 Retour du Bac blanc avec son corrigé
        * 🏊 [Rituel du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/452c-2926781) 
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  📅 Chapitre traité :    Arbres binaires de recherche  (ABR)       
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Implémentations : arbre binaire de recherche immuable (fonctionnel) ou mutable (POO), exercices 3, 4 et 5 => [Carnet Capytale de saisie des codes](https://capytale2.ac-paris.fr/web/c/b1e8-2804836)
                      * Recherche dans un ABR : exercice 6
        * Travail à faire :
            * Pour jeudi 8/02 : exercice 1 de ce [carnet](https://capytale2.ac-paris.fr/web/c/f19f-2926726), l'exercice 2 est facultatif
            * Trois exercices supplémentaires à traiter  des sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT


    ??? done "Séance 49 : jeudi 08/02/2024"
        * 🏊 correction de l'exercice 1 de ce [carnet](https://capytale2.ac-paris.fr/web/c/f19f-2926726), l'exercice 2 était facultatif
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            *  📅 Chapitre traité :    Arbres binaires de recherche  (ABR)       
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Recherche dans un ABR : exercice 6
                      * Ajout dans un ABR : exercices 7 et 8
                      *  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
        * Travail à faire :
            * Pour mardi  13/02 : trois exercices supplémentaires à traiter  des sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            * Pour jeudi 15/02 : [DM 4](./Devoirs/DM/2023-2024/DM4-Processus.pdf), lire auparavant le [rappel de cours sur les systèmes d'exploitation](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)


    ??? done "Séance 50 : vendredi 09/02/2024"
        * 🏊 Exercice rituel dans ce [carnet](https://capytale2.ac-paris.fr/web/c/065a-2959830) : parcours en largeur et arbre enraciné
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
                   *  [Synthèse sur le système d'exploitation, la ligne de commande, les logiciels libres](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)
            *  📅 Chapitre traité :    Arbres binaires de recherche  (ABR)       
                  *  [Cours avec exercices](./T1_Structures_de_données/2_Structures_arborescentes/C2_Arbre_binaire_de_recherche/P1_Définitions/P1_Définitions.md) :
                      * Ajout dans un ABR : exercices 7 et 8
                      *  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
        * Travail à faire :
            * Pour mardi  13/02 : trois exercices supplémentaires à traiter  des sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
            * Pour jeudi 15/02 : [DM 4](./Devoirs/DM/2023-2024/DM4-Processus.pdf), lire auparavant le [rappel de cours sur les systèmes d'exploitation](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)
  

    ??? done "Séance 51 : mardi  13/02/2024"
        * Séance du jour :  
            
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
                   *  [Synthèse sur le système d'exploitation, la ligne de commande, les logiciels libres](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)
            *  📅 Chapitre traité :    recherche textuelle
               *  [Cours / TP](../docs/T5_Algo/2_Recherche_textuelle/C1_Algorithmes_de_recherche_textuelle/C1_Boyer_Moore.md)
               *  [Métériel du TP à télécharger pour utilisation dans Spyder](../docs/T5_Algo/2_Recherche_textuelle/C1_Algorithmes_de_recherche_textuelle/ressources/materiel_tp_motif.zip)
            *  🏊 :    Arbres binaires de recherche  (ABR)                
                *  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096) 
            * Pour jeudi 15/02 : [DM 4](./Devoirs/DM/2023-2024/DM4-Processus.pdf), lire auparavant le [rappel de cours sur les systèmes d'exploitation](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)



    ??? done "Séance 52 : jeudi 15/02/2024"
        * Séance du jour :  
            
            * 👾  [Space war microbit sur Capytale](https://capytale2.ac-paris.fr/web/c/7663-3004349)
            * ❓ Question du jour : que désignent les sigles x86 et AMD64 ?
            *  Récolte du [DM 4](./Devoirs/DM/2023-2024/DM4-Processus.pdf) 
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
                   *  [Synthèse sur le système d'exploitation, la ligne de commande, les logiciels libres](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_Systeme.md)
            *  📅 Chapitre traité :    Systèmes sur Puce
                  *  [Cours](./T3_Archi_Sys_Réseau/C3_SOC/ressources/TNSI-SOC-Cours-2022V1.pdf)
                  *  [Corrigés](./T3_Archi_Sys_Réseau/C3_SOC/ressources/correction_soc.pdf)
            *  🏊 :    Arbres binaires de recherche  (ABR)                
                *  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096) 
            * Pour mardi 5/03 : interrogation sur processus et arbres binaires de recherche


    ??? done "Séance 53 : vendredi 16/02/2024"
        * Séance du jour :  

            * ❓ Question du jour : que désignent les sigles x86 et AMD64 ?
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
                   *  [Synthèse sur le système d'exploitation, la ligne de commande, les logiciels libres](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_Systeme.md)
            *  📅 Chapitre traité :    Systèmes sur Puce
                  *  [Cours](./T3_Archi_Sys_Réseau/C3_SOC/ressources/TNSI-SOC-Cours-2022V1.pdf)
                  *  [Corrigés](./T3_Archi_Sys_Réseau/C3_SOC/ressources/correction_soc.pdf)
            *  🏊 :    Arbres binaires de recherche  (ABR)                
                *  [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/07d4-2945524)
            * Pour s'occuper : commencer le     [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096)
        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096) 
            * Pour mardi 5/03 : interrogation sur processus et arbres binaires de recherche
        


??? bug "Mars"

    ??? done "Séance 54 : mardi 05/03/2024"
        * Séance du jour :  

            *  🏊  [Exercice sur les ABR dans Capytale](https://capytale2.ac-paris.fr/web/c/4c4f-3108018)
            * 💯  Retour sur le [DM4](./Devoirs/DM/2023-2024/DM4-Processus.pdf) et en particulier [l'ordonnancement des processus](./Devoirs/DM/2023-2024/correction-dm4.md)
            *  📰   Actualités  :               
                   *  Entrainement personnel sur les sujets d'épreuve pratique 2023 sur le site [codepuzzle.io/defis-banque](https://www.codepuzzle.io/defis-banque), une classe a été créée et les codes personnels ont été envoyés par la messagerie ENT
                   *  [Synthèse sur le système d'exploitation, la ligne de commande, les logiciels libres](./T3_Archi_Sys_Réseau/C5_Systeme_exploitation/P1_systeme/P1_Systeme.md)
            *  📅 Chapitre traité :    Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/P1_Définitions/P1_Définitions.md)
                  *  Définitions, Interface, Représentations  : Exercices 1 à 2
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/Synthèse/synthese_graphe.md)


        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096) 
            * Pour jeudi  7/03 : interrogation sur processus et arbres binaires de recherche


    ??? done "Séance 55 : jeudi 07/03/2024"
        * Séance du jour :  

            * 💯 DS sur les thèmes Processus et Arbres Binaires de Recherche
            *  📅 Chapitre traité :    Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/P1_Définitions/P1_Définitions.md)
                  *  Interface, Représentations par matrice d'adjacence  : Exercices 3 à 6
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/Synthèse/synthese_graphe.md)


        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096)


    ??? done "Séance 56 : vendredi 08/03/2024"
        * Séance du jour :  

            * 🏊  [Exercices du jour (source : Franck Chambon)](https://capytale2.ac-paris.fr/web/c/61e7-3126654)
            * 💯 Retour du DS sur les thèmes Processus et Arbres Binaires de Recherche
            *  📅 Chapitre traité :    Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/P1_Définitions/P1_Définitions.md)
                  *  Interface :
                     *   Représentations par matrice d'adjacence  : exercices 3 à 6 (correction)
                     *   Représentation par tableau de listes d'adjacences : exercices 7 à 8
                     *   Représentation par dictionnaire de listes d'adjacences : exercice 9
                     *   Comparaison des représentations : exercice 10
                     *   Implémentations en Python  de la classe Graphe : exercices 11 et 12 
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/Synthèse/synthese_graphe.md)


        * Travail à faire :
            * Pour jeudi  14/03 : [TP/DM sur les files de priorité](https://capytale2.ac-paris.fr/web/c/31c0-2974096)


    ??? done "Séance 57 : jeudi 14/03/2024"
        * Séance du jour :  

            * 🏊  [Exercices du jour : cadeaux circulaires](https://capytale2.ac-paris.fr/web/c/5ef7-3174514)
            * 📅 Chapitre traité :    Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/P1_Définitions/P1_Définitions.md)
                  *  Interface :
                     *   Implémentations en Python  de la classe Graphe : exercices 11 et 12 
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C0_Graphes/Synthèse/synthese_graphe.md)
            * 📅 Chapitre traité : Algorithmes de  Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/P1_Parcours/P1_Parcours.md)
                  *  Parcours de graphes :
                     * Parcours en largeur et exercice 13
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/Synthèse/synthese_algo_graphe.pdf)


    ??? done "Séance 58 : vendredi 15/03/2024"
        * Séance du jour :  
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://cyclades.education.gouv.fr/delos/public/listPublicECE)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🏊  [Exercices du jour : la rumeur et graphe biparti](https://capytale2.ac-paris.fr/web/c/bbf2-3188550)  
            * 📅 Chapitre traité : Algorithmes de  Graphes
                  *  [Cours](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/P1_Parcours/P1_Parcours.md)
                  *  Parcours de graphes :
                     * Parcours en profondeur et exercice 14 
                  *  [Synthèse de cours](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/Synthèse/synthese_algo_graphe.pdf)
                  * [Applications des parcours de graphe sur des graphes non orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/):
                     * [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/d6e9-3187592){: .md-button}
                     * [Exploration d'un labyrinthe](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#exploration-dun-labyrinthe-et-existence-dun-chemin-entre-deux-sommets-dun-graphe)
                     * [Composante connexe](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#bulles-informationnelles-et-composantes-connexes-dans-un-graphe-non-oriente)
            * Travail à faire :
                * Pour mardi :
                    *  Sélectionner au moins deux [entrées du programme de terminale NSI](https://eduscol.education.fr/document/30010/download) qui vous inspirent pour le _Grand oral_
                    *  Lire le diaporama de  [présentation du Grand Oral](https://nuage03.apps.education.fr/index.php/s/amf8BsMbDot9dF8) et la [FAQ du Grand Oral](https://eduscol.education.fr/document/3420/download?attachment)
                    *  Mettre en favoris dans l'ENT la [liste de ressources pour le Grand Oral](tps://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/page/view.php?id=1741) et  la [Boîte à outils pour le Grand Oral](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)


    ??? done "Séance 59 : mardi 19/03/2024"
        * Séance du jour :  
            * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/1c67-3220914)
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
 
            * 📅 Chapitre traité : Algorithmes de  Graphes
                  * [Applications des parcours de graphe sur des graphes non orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/):
                     * [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/d6e9-3187592){: .md-button}
                     * [Exploration d'un labyrinthe](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#exploration-dun-labyrinthe-et-existence-dun-chemin-entre-deux-sommets-dun-graphe)
                     * [Composante connexe](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#bulles-informationnelles-et-composantes-connexes-dans-un-graphe-non-oriente)
                  *  [Applications des parcours de graphe sur des graphes  orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#algorithmes-sur-les-graphes-orientes):
                     *  [Détection de cycles dans un graphe orienté](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#detection-dun-cycle-dans-un-graphe-de-dependances)
                     *  [Tri topologique](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#planification-de-taches-et-ordre-topologique)
                  *  [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/5a8f-2171746)
            * Travail à faire :
                * Pour jeudi :
                    *  Travail personnel : s'entraîner sur les sujets 1 à 10 de la banque d'épreuve 2023 sur la plateforme CodePuzzle, les codes ont été renvoyés lundi 18/03 sur votre messagerie ENT



    ??? done "Séance 60 :  jeudi 21/03/2024"
        * Séance du jour :  
            * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/0f6c-3241500)
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
 
            * 📅 Chapitre traité : Algorithmes de  Graphes
                  * [Applications des parcours de graphe sur des graphes non orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/):
                     * [💻 Saisir ses réponses sur Capytale](https://capytale2.ac-paris.fr/web/c/d6e9-3187592){: .md-button}
                     * [Composante connexe](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#bulles-informationnelles-et-composantes-connexes-dans-un-graphe-non-oriente)
                  *  [Applications des parcours de graphe sur des graphes  orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#algorithmes-sur-les-graphes-orientes):
                     *  [Détection de cycles dans un graphe orienté](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#detection-dun-cycle-dans-un-graphe-de-dependances)
                     *  [Tri topologique](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#planification-de-taches-et-ordre-topologique)
                  *  [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/5a8f-2171746)
            * Travail à faire :
                * Pour vendredi 29/03 :
                  * DS sur les graphes


    ??? done "Séance 61 : vendredi 22/03/2024"
        * Séance du jour :  
            * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/3f63-3253221)
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
 
            * 📅 Chapitre traité : Algorithmes de  Graphes
                  *  [Applications des parcours de graphe sur des graphes  orientés](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#algorithmes-sur-les-graphes-orientes):
                     *  [Détection de cycles dans un graphe orienté](https://fjunier.forge.aeif.fr/terminale_nsi/T1_Structures_de_donn%C3%A9es/3_Graphes/C1_Algorithmes_de_graphes/P2_Applications/P2_Applications/#detection-dun-cycle-dans-un-graphe-de-dependances)
                  *  [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/5a8f-2171746)
            * 📅 Chapitre traité : Sécurisation des communications
               * [Cours/Exercices](./T3_Archi_Sys_Réseau/C4_Sécurisation_communication/P1_Sécurisation_communication.md)    
            * Travail à faire :
                * Pour mardi 27/03 : s'entraîner à la maison sur les dix premiers exercices de la banque d'épreuve 2024
                * Pour vendredi 29/03 :
                  * DS sur les graphes


    ??? done "Séance 62 : mardi 26/03/2024"
        * Séance du jour :  
            * 💯 [Correction du TP/DM sur les files de priorité et la structure de tas](https://capytale2.ac-paris.fr/web/c/0e1a-3269802)
            * 🏊 [Exercice du jour sur Capytale :](https://capytale2.ac-paris.fr/web/c/982c-3284773) : détection de cycle et test de connexité dans un graphe non orienté simple.
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
 
            * 📅 Chapitre traité : Sécurisation des communications
               * [Cours/Exercices](./T3_Archi_Sys_Réseau/C4_Sécurisation_communication/P1_Sécurisation_communication.md)    
            * Travail à faire :
                * Pour vendredi 29/03 :
                  * DS sur les graphes


    ??? done "Séance 63 : jeudi 28/03/2024"
        * Séance du jour :  
 
            * 🏊 [Exercice du jour sur Capytale :](https://capytale2.ac-paris.fr/web/c/dba2-3305563) : recherche de doublons (Codex), arbre ternaire (Franck Chambon) et matrice d'adjacence (Codex)
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
 
            * 📅 Chapitre traité : Sécurisation des communications
                * A propos de l'échange de clef de Diffie-Hellman : [Video de David Louapre](https://ladigitale.dev/digiview/#/v/66049bdbd3d1f)
                * [Cours/Exercices](./T3_Archi_Sys_Réseau/C4_Sécurisation_communication/P1_Sécurisation_communication.md)    
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
            * Travail à faire :
                * Pour vendredi 29/03 :
                  * DS sur les graphes
  
    ??? done "Séance 64 : vendredi 29/03/2024"
         💯  DS sur les graphes (2 heures)

??? bug "Avril"
 
    ??? done "Séance 65 : mardi 02/04/2024"
        * Séance du jour :  
            * 💯  Rattrapage du DS sur les graphes (2 heures) pour les absents
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                * [Nuit du Code](https://www.nuitducode.net/) => répondre au [formulaire ENT](https://le-parc.ent.auvergnerhonealpes.fr/clubs/nuit-du-code/nuit-du-code-2024-13247.htm)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
                * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
 
            * 📅  Chapitre traité : programmation dynamique
                * [Problème du rendu de monnaie](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P1_Rendu_de_monnaie/P1_Rendu_de_monnaie.md)
                * [Exercices sur Capytale](https://capytale2.ac-paris.fr/web/c/601b-3332392)    
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
                * Présentation  et répartition des sujets
                * Traitement de l'exemple sur les arbres binaires
            * Travail à faire :
                * Pour mardi 09/04/2024
                  * Épreuve pratique blanche (durée 1 h par élève, deux vagues de 6 puis 5 élèves)


    ??? done "Séance 66 : jeudi 04/04/2024"
        * Séance du jour :  
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                * [Nuit du Code](https://www.nuitducode.net/) => répondre au [formulaire ENT](https://le-parc.ent.auvergnerhonealpes.fr/clubs/nuit-du-code/nuit-du-code-2024-13247.htm)
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
                * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
 
            * 📅  Chapitre traité : programmation dynamique
                * [Problème du rendu de monnaie](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P1_Rendu_de_monnaie/P1_Rendu_de_monnaie.md) (fin)
                * [Problème du triangle de nombres](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P2_Triangle_de_nombres/P1_Triangle_de_nombres.md)
                * [Exercices sur Capytale](https://capytale2.ac-paris.fr/web/c/601b-3332392)    
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
                * Présentation  et répartition des sujets
                * Traitement de l'exemple sur les arbres binaires
            * Travail à faire :
                * Pour mardi 09/04/2024
                  * Épreuve pratique blanche (durée 1 h par élève, deux vagues de 6 puis 5 élèves)


    ??? done "Séance 67 : vendredi 05/04/2024"
        * Séance du jour :  
            * 🏊 [Exercice du jour sur Capytale :](https://capytale2.ac-paris.fr/web/c/41f4-3363873)  : deux exercices de la plateforme [Codex](https://codex.forge.apps.education.fr/) sur la programmation dynamique.
                
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                * [Nuit du Code](https://www.nuitducode.net/) => répondre au [formulaire ENT](https://le-parc.ent.auvergnerhonealpes.fr/clubs/nuit-du-code/nuit-du-code-2024-13247.htm)
                * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
                * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
 
            * 📅  Chapitre traité : programmation dynamique
                * [Problème du triangle de nombres](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P2_Triangle_de_nombres/P1_Triangle_de_nombres.md)
                * [Exercices sur Capytale](https://capytale2.ac-paris.fr/web/c/601b-3332392)    
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
                * Présentation  et répartition des sujets
                * Traitement de l'exemple sur les arbres binaires
            * Travail à faire :
                * Pour mardi 09/04/2024
                  * Épreuve pratique blanche (durée 1 h par élève, deux vagues de 6 puis 5 élèves)
                  * Réviser à partir des exercices de la plateforme [e-nsi pratique](https://e-nsi.gitlab.io/pratique/)


    ??? done "Séance 68 : mardi 09/04/2024"
        * 🖥️ 💯  Épreuve pratique blanche



    ??? done "Séance 69 : jeudi 11/04/2024"
        * Séance du jour :  
            * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/f21f-3406391)   deux exercices de la plateforme Codex sur la programmation dynamique :  nombre de chemins dans une grille et suite de Hofstadter Figure-Figure
                
            *  📰   Actualités  : 
                *  [Inscription sur la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101)
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                * [Nuit du Code](https://www.nuitducode.net/) => répondre au [formulaire ENT](https://le-parc.ent.auvergnerhonealpes.fr/clubs/nuit-du-code/nuit-du-code-2024-13247.htm)
                * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient
                * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
 
            * 📅  Chapitre traité : programmation dynamique
                * [Problème du triangle de nombres (fin)](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P2_Triangle_de_nombres/P1_Triangle_de_nombres.md)
                * [Exercices sur Capytale](https://capytale2.ac-paris.fr/web/c/601b-3332392)  
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/c929-3151222)  
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
                * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
                * Travail en classe
            * Travail à faire :
                * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)



    ??? done "Séance 70 : vendredi 12/04/2024"
        * Séance du jour :                  
            *  📰   Actualités  : 
                * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac)
                * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
                * [Nuit du Code](https://www.nuitducode.net/) => répondre au [formulaire ENT](https://le-parc.ent.auvergnerhonealpes.fr/clubs/nuit-du-code/nuit-du-code-2024-13247.htm)
                * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
                * Objectif du jour sélectionner deux entrées du [programme de terminale NSI](https://eduscol.education.fr/document/30010/download) et leur associer un ou des thèmes de sujet de Grand Oral qui vous intéresseraient => dans le digipad sur [Elea](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
                * [Ressources Grands Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
                * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
 
            * 📅  Chapitre traité : programmation dynamique
                * [Problème de l'alignement de séquences](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P3_Alignement_de_séquences/P3_Alignement_de_sequences.md)
                * [Exercices sur Capytale](https://capytale2.ac-paris.fr/web/c/601b-3332392)  
                * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/c929-3151222)  
            * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
                * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
                * Travail en classe
            * Travail à faire :
                * Pour le mardi 30/04/2024 : déposer sa fiche de suivi pour le Grand Oral remplie sur sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
                * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)


    ??? done "Séance 71 : mardi 30/04/2024"
        * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/8cf2-3496222)   deux exercices de la plateforme Codex sur la recherche dichotomique, les piles (expression bien parenthésée) et la programmation dynamique (plus longue sous-chaine commune)
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)

        * 📅  Chapitre traité : programmation dynamique
            * [Problème de l'alignement de séquences](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P3_Alignement_de_séquences/P3_Alignement_de_sequences.md)
            * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/c929-3151222)  
        * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
            * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
            * Travail en classe
        * Travail à faire :
            * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)

??? bug "Mai"

    ??? done "Séance 72 : jeudi 02/05/2024"
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](www.pyxelstudio.net/ps/qwsyjx6b)
        * 📅  Chapitre traité : programmation dynamique
            * [Problème de l'alignement de séquences](./T5_Algo/1_Méthodes/C3_Programmation_dynamique/P3_Alignement_de_séquences/P3_Alignement_de_sequences.md) => fin
            * [TP sur Capytale](https://capytale2.ac-paris.fr/web/c/c929-3151222)  
        * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
            * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
            * Travail en classe
        * Travail à faire :
            * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)

    ??? done "Séance 73 : vendredi 03/05/2024"
         * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/9d30-3509816)   deux exercices de la plateforme [Codex](https://codex.forge.apps.education.fr) sur les carrés semi-magiques (pour travailler la POO) et la programmation dynamique (plus longue sous-chaine commune)
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)

        * 📅  Chapitre traité : paradigmes
            * [Cours / TP](https://capytale2.ac-paris.fr/web/c/7aa8-1369486) 
        * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
            * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
            * Travail en classe
        * Travail à faire :
            * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)


    ??? done "Séance 74 : mardi  07/05/2024"
         * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/a12e-3528731)   deux exercices de la plateforme [Codex](https://codex.forge.apps.education.fr) sur les pangrammes (chaînes de caractères, tableau ou dictionnaire) et les arbres d'expressions arithmétiques (récurisivité, arbre binaire)
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)

        * 📅  Chapitre traité : paradigmes
            * Donner au moins trois caractéristiques et un langage emblématique pour les paradigmes *impératif*, *objet* et *fonctionnel* => travail au brouillon puis correction en commun dans ce [digipad](https://digipad.app/p/761611/f4596edca3f5e)
        * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
            * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)
        * [Projet Final](./Projets/P4_projet_final/projet_final2024.md)
            * Inscription dans ce [fichier](https://nuage03.apps.education.fr/index.php/s/rNYMn689Ctc9C8b)
            * Travail en classe
        * Travail à faire :
            * Pour le dimanche 12/05/2024 : déposer son projet final sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=16243)
            * Pour le jeudi 16/05 : DS sur les chapitres Sécurisation des communications, Programmation dynamique et Paradigmes (question de cours)


    ??? done "Séance 75 : mardi  14/05/2024"
        * 📅    [Synthèse du cours Paradigmes de programmation](https://digipad.app/p/767233/73c8cf8f5ab04)
        * 🏊 [Exercice du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/517e-3558886)   : chiffrement de Vernam, inspiré d'un exercice de la plateforme [Codex](https://codex.forge.apps.education.fr)
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
        * 📝 Révisions des épreuves écrites : [Exercice 1 du sujet 0.b](https://pixees.fr/informatiquelycee/term/suj_bac/2024/sujet_02.pdf) => thème traité _Programmation dynamique_ => [Un corrigé](https://nuage03.apps.education.fr/index.php/s/fajn4pgGom56PCQ)
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)

    
        * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
            * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)

        * Travail à faire :
     
            * Pour le jeudi 16/05 : DS sur les chapitres Sécurisation des communications, Programmation dynamique et Paradigmes (question de cours)
  

    ??? done "Séance 76 : jeudi 16/05/2024"
        * 💯 DS sur la sécurisation des communications et la programmation dynamique
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)

    
        * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
            * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)


    ??? done "Séance 77 : vendredi 17/05/2024"
        * 📰   Actualités  : 
            * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
            * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
            * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
            * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
            * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
        * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
            * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
            * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
            * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
         * 📅  Chapitre traité : calculabilité
            * [Programme comme donnée](./T4_Programmation/C5_Calculabilité/P1_Programme_comme_donnée/P1_Programme_comme_donnée.md)
            * [Entscheidungsproblem](./T4_Programmation/C5_Calculabilité/P2_Calculabilité_et_décidabilité/P2_Calculabilité_et_décidabilité.md)
        * [TP Graphes](https://capytale2.ac-paris.fr/web/c/5a8f-2171746)
        * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
            * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)
        * Travail à faire :
            * Jeudi prochain : DS sur les chapitres SGBD et SQL + Paradigmes de programmation 
  

    ??? done "Séance 78 : mardi 21/05/2024"
          * 🏊 [Exercices du jour sur Capytale](https://capytale2.ac-paris.fr/web/c/2f29-3602197)   : insertion dans une liste triée, recherche dichotomique itérative, somme maximale d k termes consécutifs, chemins externes dans un arbre binaire, exercices  de la plateforme [Codex](https://codex.forge.apps.education.fr)
          * 💯  : [Correction du DS 11](https://nuage03.apps.education.fr/index.php/s/JJWCjoEJaciyRko)
          * 📰   Actualités  : 
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          * 📅  Chapitre traité : calculabilité
              * [Entscheidungsproblem](./T4_Programmation/C5_Calculabilité/P2_Calculabilité_et_décidabilité/P2_Calculabilité_et_décidabilité.md)
              * [Problème de l'arrêt](./T4_Programmation/C5_Calculabilité/P3_Problème_de_l_'_arrêt/P3_Problème_de_l_'_arrêt.md)
          * [Compilation d'exercices sur les graphes non pondérés sur Capytale](https://capytale2.ac-paris.fr/web/c/7074-3604558)
          * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
              * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)
          * Travail à faire :
              * Jeudi 23/05 : DS sur les chapitres SGBD et SQL + Paradigmes de programmation


    ??? done "Séance 79 : jeudi 23/05/2024"
          * 💯  : DS 11 sur SGBD/SQL et Paradigmes de programmation
          * 📰   Actualités  : 
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          * [Compilation d'exercices sur les graphes non pondérés sur Capytale](https://capytale2.ac-paris.fr/web/c/7074-3604558)
          * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
              * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)


    ??? done "Séance 80 : vendredi 24/05/2024"
          * 📰   Actualités  : 
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (15 minutes pas plus) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite :
              * [Sujet 1 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/eCZwXZB6yC3W5Bx)
              * [Notes de correction du Sujet 1 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/4Q9iS7Lpp3mippx)
          * [Compilation d'exercices sur les graphes non pondérés sur Capytale](https://capytale2.ac-paris.fr/web/c/7074-3604558)
          * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
              * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)


    ??? done "Séance 81 : mardi 28/05/2024"
          * 🏊 [Exercices du jour sur Capytale (30 minutes)](https://capytale2.ac-paris.fr/web/c/c756-3660996)   :  files, recherche dichotomique récursive et itérative, somme maximale d k termes consécutifs, chemins 
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              *  Sujet 1 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/eCZwXZB6yC3W5Bx)
                 * [Notes de correction du Sujet 1 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/4Q9iS7Lpp3mippx)
              * Sujet 2 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/pnEdqEpNMXpS3S4)
                 * [Notes de correction du Sujet 2 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/cpBdLHXLsnzZYMt) 
          * [Compilation d'exercices sur les graphes non pondérés sur Capytale](https://capytale2.ac-paris.fr/web/c/7074-3604558)
          * 🎮 Préparation de la [Nuit du Code](https://www.nuitducode.net/) :
              * [Projet Pacman](./Projets/P5_pacman/projet_pacman.md)


    ??? done "Séance 82 : jeudi 30/05/2024"
          * 🏊 [Exercices du jour sur Capytale (30 minutes)](https://capytale2.ac-paris.fr/web/c/32b5-3678375)   :  Boucles, Piles, POO, Programmation dynamique et Arbres binaires
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              *  Sujet 1 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/eCZwXZB6yC3W5Bx)
                 * [Notes de correction du Sujet 1 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/4Q9iS7Lpp3mippx)
              * Sujet 2 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/pnEdqEpNMXpS3S4)
                 * [Notes de correction du Sujet 2 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/cpBdLHXLsnzZYMt) 
          * [Compilation d'exercices sur les graphes non pondérés sur Capytale](https://capytale2.ac-paris.fr/web/c/7074-3604558)



    ??? done "Séance 83 : Vendredi 31/05/2024"
          * 🏊 [Exercices du jour sur Capytale (30 minutes)](https://capytale2.ac-paris.fr/web/c/32b5-3678375)   :  Boucles, Piles, POO, Programmation dynamique et Arbres binaires
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              *  Sujet 1 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/eCZwXZB6yC3W5Bx)
                 * [Notes de correction du Sujet 1 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/4Q9iS7Lpp3mippx)
              * Sujet 2 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/pnEdqEpNMXpS3S4)
                 * [Notes de correction du Sujet 2 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/cpBdLHXLsnzZYMt) 
          * [Graphes : TP : algorithme de Dijkstra](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/P3_TP/materiel_tp_dijkstra.zip)
     
??? bug "Juin"

    ??? done "Séance 84 : Mardi 4/06"
          * 🏊 [Exercices du jour sur Capytale (30 minutes)](https://capytale2.ac-paris.fr/web/c/0d2d-3709052)   :  Boucles, Dictionnaires, Chaines, Arbres binaires, Piles, POO, Programmation dynamique
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          * [Graphes : TP : algorithme de Dijkstra](./T1_Structures_de_données/3_Graphes/C1_Algorithmes_de_graphes/P3_TP/materiel_tp_dijkstra.zip)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              * Sujet 2 Amérique du Nord mai 2024 :
                 * [Énoncé](https://nuage03.apps.education.fr/index.php/s/pnEdqEpNMXpS3S4)
                 * [Notes de correction du Sujet 2 Amérique du Nord mai 2024](https://nuage03.apps.education.fr/index.php/s/cpBdLHXLsnzZYMt)
  

    ??? done "Séance 84 : Jeudi 6/06"
          * 🏊 [Exercices du jour sur Capytale (30 minutes)](https://capytale2.ac-paris.fr/web/c/0d2d-3709052)   :  Boucles, Dictionnaires, Chaines, Arbres binaires, Piles, POO, Programmation dynamique
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              * Sujet de bac blanc de Germain Becker
          * [TP Graphes](https://capytale2.ac-paris.fr/web/c/5a8f-2171746)


    ??? done "Séance 85 : Vendredi 7/06"
          * 📰   Actualités  : 
              * [Sujets de DS 2023/2024](./Bac/C3_DS/C3_DS.md)
              * [Sujets de la banque d'épreuves publique pour l'EP du bac NSI](https://fjunier.forge.aeif.fr/terminale_nsi/Bac/C1_Epreuves_terminales/C1_Epreuves_terminales/#epreuve-pratique-du-bac) tous les sujets sont désormais dans CodePuzzle
              * [Présentation du Grand Oral](./Bac/C1_Epreuves_terminales/C1_Epreuves_terminales.md)
              * [Synopsis de l'ensemeble des synthèses de cours](./Bac/C2_Synthèses/C3_Synthèses.md)
              * Nouvelle plateforme [Codex](https://codex.forge.apps.education.fr/) d'entrainement au code
              * [Nuit du Code le 29 mai](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/) => un [exemple des possibilités de pyxel](https://www.pyxelstudio.net/ps/qwsyjx6b)  => l'IDE conseillé (version portable pour clef USB) est [Edupyter](https://www.edupyter.net/)
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              * [Sujet Centres-Etrangers J1](https://nuage03.apps.education.fr/index.php/s/Y3k9PnSQ6G8aY2o) et [notes de correction, sauf l'exo 1 corrigé au tableau](https://nuage03.apps.education.fr/index.php/s/2LAYEiDcTgAaoJx)
              * [Sujet Centres-Etrangers J2](https://nuage03.apps.education.fr/index.php/s/XR9XDKZnQsGdW7x) et [notes de correction](https://nuage03.apps.education.fr/index.php/s/esoJYfLcQXm7d9g)
           
          * Pas de NSI au lycée du Parc en terminale l'an prochain pour cause de sous-effectif, on revient dans deux ans !
  
          ![alt](http://images2.fanpop.com/image/photos/13700000/Back-to-the-Future-back-to-the-future-13786661-1600-1200.jpg){.center} 
        
          

    ??? done "Séance 86 : révisions mardi 10/06"
          * 🗣️   Préparation au Grand Oral (1 passage :  20 minutes) :
              * Point étape à partir de la fiche de suivi sur la [plateforme Elea](https://aura-69-metro2.elea.apps.education.fr/mod/assign/view.php?id=19135)
              * [Ressources Grand Oral sur  la plateforme Eléa](https://aura-69-metro2.elea.apps.education.fr/course/view.php?id=101&section=1)
              * [Boite à idées](https://0690026d.moodle.ent.auvergnerhonealpes.fr/mod/board/view.php?id=1844)
          *  Préparation de l'épreuve écrite  (60 minutes) :
              * [Sujet Centres-Etrangers J2](https://nuage03.apps.education.fr/index.php/s/XR9XDKZnQsGdW7x) et [notes de correction](https://nuage03.apps.education.fr/index.php/s/esoJYfLcQXm7d9g)
              * [Sujet Centres Etrangers J2 2023](https://pixees.fr/informatiquelycee/term/suj_bac/2023/sujet_04.pdf) et [notes de correction de David Roche](https://pixees.fr/informatiquelycee/term/suj_bac/2023/correction_sujet_04.pdf)


    ??? done "Séance 87 : révisions vendredi 14/06"
        *  Préparation de l'épreuve écrite  (60 minutes) :
            * [Sujet Asie J1](https://nuage03.apps.education.fr/index.php/s/xZ8sYqNNbL6xGm7) et [notes de correction](https://nuage03.apps.education.fr/index.php/s/E7Yeq3CRxpWFZDm)
            * [Sujet Asie J2](https://nuage03.apps.education.fr/index.php/s/jaXr5Qzw9RcicoT) et [notes de correction](https://nuage03.apps.education.fr/index.php/s/pCcfpQ2kLFrAyCS)
          