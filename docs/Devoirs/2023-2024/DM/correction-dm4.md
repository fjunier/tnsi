---
title:  Correction du DM n°4
---

# Correction du DM 4


## Question 1

Parmir les licences de logiciels, on distingue : 

- Les **licences propriétaires** :  
    Un éditeur concède à titre non exclusif un droit d'usage sur un     logiciel dont il conserve les droits de propriété intellectuelle. Un     **CLUF** (Contrat Licence Utilisateur Final) délimite les conditions     d'usage du logiciel : limitation du nombre de copies, de postes sur     lequel le logiciel peut être installé, interdiction de modification     et de redistribution . En général, le code source n'est pas     accessible (on parle de logiciel fermé) ...

- Les **licences libres** ou **open-source** :   elles respectent les quatre libertés fondamentales du logiciel libre     :

    1.  *Liberté 1* **liberté d'exécuter le programme, pour tous les   usages.**
    2.  *Liberté 2* **liberté d'étudier le fonctionnement du programme,   et de le modifier pour l'adapter à ses besoins**
    3.  *Liberté 3* **liberté de redistribuer des copies.**
    4.  *Liberté 4* **liberté de redistribuer aux autres des copies de    versions modifiées**



## Question 2

Un *système d'exploitation* est un logiciel, ou ensemble de programmes, qui sert d'interface entre les programmes exécutés par l'utilisateur et les ressources matérielles d'un ordinateur. C'est le premier programme chargé en mémoire au démarrage de l'ordinateur.


Un *système d'exploitation*  possède un certain nombre d'attributs qui le distinguent comme chef d'orchestre des autres programmes :

* le système d'exploitation possède un mode d'exécution privilégiée : le *mode noyau*, qui lui donne un accès unique et total aux ressources matérielles. Les programmes en cours d'exécution, appelés *processus*, accèdent aux ressources par le biais d'appels au système d'exploitation ;
* en particulier, le *système d'exploitation* gère le partage de l'accès au processeur des processus en cours, c'est *l'ordonnnancement* ; 
* le *système d'exploitation* gère les droits et permissions sur les répertoires et fichiers stockés dans l'arborescences du système de fichiers.

## Question 3


![alt](https://nuage03.apps.education.fr/index.php/s/GngYSNCS7KbWnat/download/arborescence.png))

Le chemin absolu du fichier `rapport.odt` est `/home/elsa/documents/boulot/rapport.odt`


## Question 4

Depuis le répertoire `elsa`, le chemin relatif du fichier `photo_1.jpg` est  `../max/images/photos_vac/photo_1.jpg`.



## Question 5

Après avoir exécuté depuis le répertoire `elsa`, la commande `cp documents/fiche.ods documents/boulot`, le fichier `fiche.ods` du répertoire `documents`  a été copié, sous le même nom, dans le répertoire dans le répertoire `boulot` de chemin relatif `documents/boulot`.  Le répertoire `documents` contient toujours le fichier `fiche.ods`  et le répertoire `boulot` contient une copie nommée également `fiche.ods`.



## Question 6


Voici le cycle de vie d'un processus :



![alt](https://nuage03.apps.education.fr/index.php/s/W8ajnbdAaA62me6/download/cycle_vie_processus_correc.png)

Notez que dans ce schéma, un processus élu ne peut être interrompu que s'il est bloqué. La politique d'ordonnancement est donc *non préemptive*.

## Question 7


Un processus peut être bloqué s'il attend la libération d'une ressource pour une lecture ou une écriture : par un exemple un périphérique de mémoire externe comme un disque dur.


## Question 8

Une structure de donnnées linéaire *LIFO (Last In First Out)* est la Pile.


## Question 9

Le tableau suivant présente les instants d'arrivées et les durées d'exécution de cinq processus :

![alt](https://nuage03.apps.education.fr/index.php/s/kwnEgEo48LL7RRi/download/ordonnancement_ordre_soumission.png)

L'algorithme   *par ordre de soumission* est un algorithme d'ordonnancement non préemptif. L'ordonnanceur place les processus qui ont besoin d'un accès au processeur dans
une file, en respectant leur ordre d'arrivée (le premier arrivé étant placé en tête de file). Dès qu'un processus a terminé son exécution, l'ordonnanceur donne l'accès au processus suivant dans la file. Voici l'ordonnancement des cinq processus précédents avec cet algorithme :

![alt](https://nuage03.apps.education.fr/index.php/s/He2R3HjMw9GFAFJ/download/dm4_ordonnancement1.png)

## Question 10

On utilise maintenant un algorithme d'ordonnancement *préemptif*  : l'algorithme d'ordonnancement "par tourniquet". Dans cet algorithme, la durée  l'exécution d'un processus ne peut pas dépasser une durée Q appelée quantum et fixée à l'avance.

Si ce processus a besoin de plus de temps pour terminer son exécution, il doit retourner dans la file et attendre son tour pour poursuivre son exécution.

Par exemple, si un processus P1 a une durée d'exécution de 3 et que la valeur de Q a été fixée à 2, P1 s'exécutera pendant deux unités de temps avant de retourner à la fin de la file pour attendre son tour ; une fois à nouveau élu, il pourra terminer de s'exécuter pendant sa troisième et dernière unité de temps d'exécution.

Avec ce nouvel algorithme, on refait un *chronogramme* d'utilisation du processeur avec les mêmes processus que dans la question 9. Le *quantum* de temps est fixé à 2.


|Instant (début de  cycle)|Quantum|Processus dans la file d'attente|Processus élu|
|:---:|:---:|:---:|:---:|
|0|1|début::fin|P1|
|1|1|début:P2:fin|P1|
|2|2|début:P1:fin|P2|
|3|2|début:P1:fin|P2|
|4|3|début:P3:P2:fin|P1|
|5|4|début:P2:fin|P3|
|6|4|début:P2:P4:fin|P3|
|7|5|début:P4:P5:P3:fin|P2|
|8|5|début:P4:P5:P3:fin|P2|
|9|6|début:P5:P3:P2:fin|P4|
|10|6|début:P5:P3:P2:fin|P4|
|11|7|début:P3:P2:fin|P5|
|12|8|début:P2:fin|P3|
|13|8|début:P2:fin|P3|
|14|9|début::fin|P2|
|15|9|début::fin|P2|

![alt](https://nuage03.apps.education.fr/index.php/s/G9jWecDoDHJYYxR/download/DM4-ordonnancement2.png)


## Question 11

On considère deux processus P1 et P2, et deux ressources R1 et R2.

On fait trois hypothèses : 

* (H1) :  Dans le cours de son exécution, le processus P1 a besoin d'acquérir la ressource R1 en accès exclusif en posant un verrou, puis la ressource R2 en accès exclusif. De plus  P1 ne libère les verroux sur R1 et R2 que lorsqu'il a acquis les deux ressources et terminé son traitement.
* (H2) : De façon symétrique, dans le cours de son exécution, le processus P2 a besoin d'acquérir les mêmes ressources que P1 mais dans l'ordre inverse : d'abord  R2 en accès exclusif en posant un verrou puis R1. De plus  P1 ne libère les verroux sur R2  et R1 que lorsqu'il a acquis les deux ressources et terminé son traitement.
* (H3): au cours de l'ordonnancement préemptif, P1 verrouille R1, P1 est préempté par l'ordonnanceur, puis P2 verrouille R2 avant que P1 puisse le faire. 

Sous ces trois hypothèses, un chronogramme comme ci-dessous aboutit  à un *interblocage* : P1 détient R1 et  attend R2 alors que  P2 détient  R2 et attend R1. 


![alt](https://nuage03.apps.education.fr/index.php/s/Y7nfZsJQkCpCxjH/download/interblocage.png.png)


Cet interblocage se traduit par un *cycle* dans le graphe de dépendances des processus :

![alt](https://nuage03.apps.education.fr/index.php/s/SrxYD6oCaq7CLPf/download/graphe_dependance.png)