def somme_max(t):
    """Renvoie la somme maximale dans un triangle de nombres
    t qui est une liste de listes"""
    
    def s(i, j):
        """Fonction auxiliaire récursive"""
        if i == len(t) - 1:
            return ...
        return ...
    
    return s(0, 0)

def test_somme_max():
    t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
    assert somme_max(t) == 137
    print("tests réussis")
    
