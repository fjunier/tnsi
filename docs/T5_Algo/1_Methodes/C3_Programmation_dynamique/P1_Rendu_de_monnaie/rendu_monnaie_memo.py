def rendu_monnaie_memo(montant, pieces):
    """Renvoie le nombre minimal de pièces pour rendre la monnaie
    sur montant avec le système monétaire pieces qui contient une pièce de 1"""
    
    
    def rendu_monnaie(montant, pieces):
        if montant not in memo:
            rmin = montant # montant pièces de 1, pire des cas
            for p in pieces:
                if p <= montant:
                    rmin = min(rmin, 1 + rendu_monnaie(montant - p, pieces))
            # à compléter
            ...
        return ...
    
    memo = {0: 0}
    return rendu_monnaie(montant, pieces)


def test_rendu_monnaie_memo():
    assert rendu_monnaie_memo(10, [1, 2]) == 5
    assert rendu_monnaie_memo(8, [1, 4, 6]) == 2
    systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    assert rendu_monnaie_memo(49, systeme_euro) == 5
    assert rendu_monnaie_memo(76, systeme_euro) == 4
    print("tests réussis")
    
    
#test_rendu_monnaie_memo()