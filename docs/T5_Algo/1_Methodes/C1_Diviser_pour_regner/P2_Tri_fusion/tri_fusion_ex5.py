def fusion(t1, t2):
    """
    Fusionne les tableaux d'entiers  t1 et t2 triés dans l'ordre croissant en un tableau  t3 trié dans l'ordre croissant  et constitué des mêmes    éléments  que t1 et t2  
    """
    n1 = len(t1)
    n2 = len(t2)
    t3 = []
    i1, i2 = 0, 0
    while i1 < n1 and ...:
        # à compléter
        ...
    # ici un des deux tableaux t1 ou t2 est vide
    # cas il reste t1 non vide
    while i1 < n1:
        # à compléter
        ...
    # cas il reste t2 non vide
    while i2 < n2:
        # à compléter
        ...
    return t3

def test_fusion():
    """
    Jeu de tests unitaires pour  la fonction fusion
    de signature  fonc_fusion(t1, t2)
    qui fusionne les tableaux t1 et t  dans l'ordre croissant
    en un tableau t3 dans l'ordre croissant
    """
    assert fusion([1, 4], [3]) == [1, 3, 4]
    assert fusion([1, 4], []) == [1,4]
    assert fusion([1, 4], [0, 5]) == [0, 1, 4, 5]
    print("tests réussis")