import time

def maximum_dpr(tab):
    """
    Renvoie le maximum d'un tableau d'entiers
    Algorithme récursif avec méthode Diviser pour Régner
    """
    # à compléter
    ...
    
def test_doubling_ratio(bsup):
    """
    Affiche l'évolution du  ratio du temps d'exécution de maximum_dpr
    lorsqu'on double la taille de l'entrée à chaque nouvel essai
    """
    taille = 1000
    temps_preced = 0
    while taille < bsup:
        tab = [0 for _ in range(taille)]
        debut = time.perf_counter()
        maximum_dpr(tab)
        temps = time.perf_counter() - debut
        if temps_preced == 0:
            ratio = 1
        else:
            ratio = temps / temps_preced
        print(f"Taille(n):{taille}|Ratio taille:2|Temps(n):{temps}|Ratio temps:{ratio}")
        temps_preced = temps
        taille *= 2