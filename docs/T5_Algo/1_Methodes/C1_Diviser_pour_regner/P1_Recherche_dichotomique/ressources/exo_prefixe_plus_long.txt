

!!! question "Exercice 3"
    > source : Guillaume Connan [cours TNSI 2022](https://nsind.gitlab.io/tale/2020_21/CHAP_8/8_diviser20/)

    !!! success "Question 1"

        === "énoncé"
            On suppose qu'on dispose d'une fonction `prefixe_max2(c, d)` qui renvoie le plus long préfixe  commun à deux chaînes de caractères `c` et `d`.  

            ~~~python
            >>> prefixe_max2('gact', 'gatt')
            'ga'
            >>> prefixe_max2('gact', 'gacg')
            'gac'
            >>> prefixe_max2('gact', 'gactg')
            'gact'
            >>> prefixe_max2('gact', 'gactg')
            'gact'
            >>> prefixe_max2('gact', 'aact')
            ''
            >>> prefixe_max2('gact', '')
            ''
            ~~~

            Décrivez en français un algorithme récursif de type _Diviser pour Régner_  qui permette de déterminer le plus long préfixe commun pour un tableau de chaînes de caractères. Mettez en évidence les trois étapes _Diviser_, _Résoudre_ et _Combiner_ d'un algorithme de type _Diviser pour Régner_.

        === "solution"

            à faire


    !!! success "Question 2"

        === "énoncé"
            Complétez le code Python de la fonction `prefixe_max2` spécifiée ci-dessous.

            {{IDE('prefixe_max')}}

        === "solution"

            ~~~python
            def prefixe_max2(c, d):
                """
                Renvoie le préfixe commun le plus long
                aux deux chaînes de caractères c et d
                """
                p = ""
                i = 0 
                while (i < len(c)) and (i < len(d)) and (c[i] == d[i]):
                    p = p + c[i]
                    i = i + 1
                return p
            ~~~
            

    !!! success "Question 3"

        === "énoncé"
            Complétez le code Python de la fonction `prefixe_max_tab` spécifiée dans l'éditeur précédent. 
            Mettez en évidence les trois étapes _Diviser_, _Résoudre_ et _Combiner_ d'un algorithme de type _Diviser pour Régner_.

        
        === "solution"

            ~~~python
            def prefixe_max_tab(t):
                """
                Renvoie le préfixe commun le plus long
                à toutes les chaînes de caractères du tableau t
                """
                if len(t) == 1:
                    return t[0]
                if len(t) == 0:
                    return ''
                # Etape diviser
                # on divise le tableau au milieu en deux                 
                m = len(t) // 2
                # Etape résoudre
                # et on résout le problème pour les deux sous-tableaux 
                p1 = prefixe_max_tab(t[:m])
                p2 = prefixe_max_tab(t[m:])
                # Etape combiner
                return prefixe_max2(p1, p2)
            ~~~
