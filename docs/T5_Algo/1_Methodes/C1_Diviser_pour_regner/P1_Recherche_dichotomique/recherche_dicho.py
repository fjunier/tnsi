def recherche_dicho_iter(t, e):
    """
    Recherche dichotomique dans un tableau trié dans l'ordre croissant
    Algorithme itératif avec une boucle
   
    Paramètres :
        t : tableau d'éléments  de même type et comparables
              précondition : t trié dans l'ordre croissant
        e : un élément du même type que ceux  dans tab

    Retour:
        Si e dans tab renvoie l'index d'une occurrence
        Sinon renvoie -1
    """
    g, d = 0, len(t) - 1
    while g <= d:
        m = (g + d) // 2
        # Sous-problème 1 :  occurrence de e en t[m]
        if e == t[m]:
            return m
        # Sous-problème 2 :  occurrence de e en t[m]
        #  on continue la recherche dans la première moitié [g, m [ = [g, m - 1]          
        # à compléter
        ...   
        # Sous problème 3
        # on continue la recherche dans la seconde moitié [m + 1, d]        
        # à compléter
        ... 
    return ...

def recherche_dicho_rec(t, e, g, d):
    """
    Recherche dichotomique dans un tableau trié dans l'ordre croissant
    Algorithme récursi
   
    Paramètres :
        t : tableau d'éléments  de même type et comparables
              précondition : t trié dans l'ordre croissant
        e : un élément du même type que ceux  dans tab
        g : index limite à gauche de la zone de recherche
        d : index limite à droite de la zone de recherche

    Retour:
        Si e dans tab renvoie l'index d'une occurrence
        Sinon renvoie -1
    """
    m = (g + d) // 2
    # 1er cas de base : zone de recherche vide
    # à compléter
    ... 
    # Sous-problème 1
    # 2eme cas de base :  occurrence de e en t[m]
    # à compléter
    if t[m] == e:
        return ...
    # Sous problème 2
    # on continue la recherche dans la première moitié [g, m [ = [g, m - 1]
    # à compléter
    elif e < t[m]:
        return recherche_dicho_rec(t, e, ..., ...)
    # Sous problème 3
    # on continue la recherche dans la seconde moitié [m + 1, d]
    # à compléter
    ... 

def recherche_dicho_rec_env(t, e):
    """Fonctione enveloppe pour  la recherche dichotomique récursive 
    de l'élément e dans le tableau. 
    Masque le passage des arguments représentant les limites de la zone 
    de recherche    
    """
    # à compléter
    return recherche_dicho_rec(..., ..., ..., ...)

def test_recherche_dicho(fonction):
    """Jeu de tests unitaire pour une  fonction de recherche
    de recherche dichotomique dans un tableau trié dans l'ordre croissant
    dont la signature est fonction(tableau, element)
    """
    t1 = [-4, 0, 1, 5, 9, 17, 42, 100]
    assert fonction(t1, 9) == 4, "échec pour t1 et 4"
    assert fonction(t1, -4) == 0, "échec pour  t1 et -4"
    assert fonction(t1,  100) == 7, "échec pour  t1 et 100"
    assert fonction(t1,  6) == -1, "échec pour  t1 et 6"
    assert fonction(t1,  101) == -1, "échec pour  t1 et 100"
    assert fonction(t1,  -6) == -1, "échec pour  t1 et -6"
    t2 = [-4]
    assert fonction(t2,  -4) == 0, "échec pour  t2 et -4"
    assert fonction(t2,  1) == -1, "échec pour  t2 et 1"
    t3 = []
    assert fonction(t3,  0) == -1, "échec pour  t3 et 0"
    print("tests réussis")

# test de la recherche dichotomique itérative
#test_recherche_dicho(recherche_dicho_iter)

# test de la recherche dichotomique récursive
#test_recherche_dicho(recherche_dicho_rec_env)