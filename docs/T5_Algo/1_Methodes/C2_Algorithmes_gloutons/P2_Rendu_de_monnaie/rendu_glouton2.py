def rendu_rec(restant, pieces, indice_pieces):
    # pieces contient les valeurs des pièces disponibles par ordre croissant
    if restant == 0:
        rep = []
    elif pieces[indice_pieces] <= restant:
        rep = ...
        rep.append(pieces[indice_pieces])                
    else:
        rep = ...
    return rep

def rendu_glouton2(restant, pieces):
    return rendu_rec(restant, pieces, len(pieces) - 1)

def test_rendu_glouton2():
    systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    assert rendu_glouton2(76, systeme_euro) == [1, 5, 20, 50]
    assert rendu_glouton2(49, systeme_euro) == [2, 2, 5, 20, 20]
    assert rendu_glouton2(843, systeme_euro) == [1, 2, 20, 20, 100, 200, 500]
    systeme_non_canonique = [1, 3, 6, 12, 24, 30]
    assert rendu_glouton2(49 , systeme_non_canonique) == [1, 6, 12, 30]
    assert rendu_glouton2(53 , systeme_non_canonique) == [1, 1, 3, 6, 12, 30]
    print("tests réussis pour rendu_glouton2")
    
#test_rendu_glouton2()