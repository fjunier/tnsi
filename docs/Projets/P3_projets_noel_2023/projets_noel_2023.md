---
title: Projet Noël 2023
---

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


## Présentation

On commence le projet en classe avec une répartition en quatre binômes et un trinôme.

Cinq sujets sont proposés dans le carnet Capytale en lien ci-dessous :

|Niveau de difficulté|Sujet|Thèmes abordés|
|:---:|:---:|:---:|
|1|transformation du photomaton|Tableaux Python, fonctions, boucles|
|2|approximation d'une courbe de Bézier quadratique|Diviser Pour Régner, Récursivité|
|3|chiffrement/déchiffrement avec une scytale|Chiffrement symétrique, Structures de données linéaires (files)|
|4|codage d'une image avec un arbre quaternaire|Codage, Récursivité, Structures de données  arborescentes| 
|5|génération et exploration de labyrinthe|CGraphes, Algorithmes sur les graphes,  Structures de données linéaires (piles et files)| 


L'évaluation portera sur la présentation orale du projet selon trois critères : qualité du contenu (3 points), qualité de la prise de parole en continu (2 points), qualité de la posture (1 point).

## Projet

*  [💻 Mini projet Noël 2023 sur Capytale](https://capytale2.ac-paris.fr/web/c/df20-2311094){: .md-button}
