import sys

sys.setrecursionlimit(5000)


def nb_sommes(n):
    def aux(i, j):
        if (i, j) not in memo:
            if i <= 1:
                memo[(i, j)] = 1
            else:
                s = 0
                for k in range(1, min(j, i) + 1):
                    s = s + aux(i - k, k)
                memo[(i, j)] = s
        return memo[(i, j)]

    memo = dict()
    rep = aux(n, n)
    return rep


partition_mem = dict()


def nb_sommes2(n):
    def nb_k_sommes(n, k):
        if (n, k) not in partition_mem:
            if n < k:
                resultat = 0
            elif k == 1:
                resultat = 1
            else:
                resultat = nb_k_sommes(n - 1, k - 1) + nb_k_sommes(n - k, k)
            partition_mem[(n, k)] = resultat
        return partition_mem[(n, k)]

    return sum(nb_k_sommes(n, k) for k in range(1, 1 + n))


# tests

assert nb_sommes(3) == 3
assert nb_sommes(5) == 7
