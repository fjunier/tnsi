def evenements(trains):
    les_evenements = []
    for (arrivee, depart) in trains:
        les_evenements.append((arrivee, 1))
        les_evenements.append((depart, -1))
    return les_evenements


def nb_maxi_quais(trains):
    les_evenements = evenements(trains)
    les_evenements.sort() 

    nb_quais_occupes = 0
    maxi = 0
    for (horaire, variation) in les_evenements:
        nb_quais_occupes += variation
        if nb_quais_occupes > maxi:
            maxi = nb_quais_occupes
    return maxi


# Tests
trains = [(3, 5), (2, 4), (6, 8)]
assert sorted(evenements(trains)) == [(2, 1), (3, 1), (4, -1), (5, -1), (6, 1), (8, -1)]
assert nb_maxi_quais(trains) == 2

trains = [(1, 3), (6, 7), (5, 6), (3, 5)]
assert sorted(evenements(trains)) == [(1, 1), (3, -1), (3, 1), (5, -1), (5, 1), (6, -1), (6, 1), (7, -1)]
assert nb_maxi_quais(trains) == 1
