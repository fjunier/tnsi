# la classe Pile est utilisable directement pour cet exercice

def filtre_positifs(donnees):
    "fonction à compléter"
    auxiliaire = Pile()
    while not donnees.est_vide():
        element = donnees.depile()
        auxiliaire.empile(element)
    
    resultat = Pile()
    while not auxiliaire.est_vide():
        element = auxiliaire.depile()
        donnees.empile(element)
        if element >= 0:
            resultat.empile(element)
    
    return resultat





# tests

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == '| 4 | 7 | 0 | 6 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet'

donnees = Pile([1, 2, 3, 4])
assert str(filtre_positifs(donnees)) == '| 1 | 2 | 3 | 4 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 1 | 2 | 3 | 4 <- sommet'

donnees = Pile([-4, -3, -2, -1])
assert str(filtre_positifs(donnees)) == '|  <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| -4 | -3 | -2 | -1 <- sommet'

