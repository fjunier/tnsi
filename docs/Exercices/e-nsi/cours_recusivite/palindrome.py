def est_palindrome(mot):
    if len(mot) < 2:
        return True
    return mot[0] == mot[1] and est_palindrome(mot[1:-1])
    
assert est_palindrome('mot') == False
assert est_palindrome('tot') == True
assert est_palindrome('esse') == True
