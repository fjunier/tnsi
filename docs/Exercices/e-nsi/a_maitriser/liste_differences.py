def differences(source_1, source_2):
    """à compléter"""
    assert len(source_1) == len(source_2)
    return [source_1[i] != source_2[i] for i in range(len(source_1))]
        

# tests

assert differences([14, 87, 22, 5, 65],
                   [14, 86, 27, 5, 65]) == [False, True, True, False, False]

assert differences([-54], [-54]) == [False]
assert differences([7, 8], [7, 11]) == [False, True]
assert differences([], []) == []

