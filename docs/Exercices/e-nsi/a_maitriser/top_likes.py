#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 14:45:59 2023

@author: fjunier
"""

def top_likes(likes):
    maxi = -1
    for personne in likes:
        nb_likes = likes[personne]
        if nb_likes > maxi:
            maxi = nb_likes
            pmaxi = personne
        elif nb_likes == maxi:
            if personne < pmaxi:
                pmaxi = personne
    return (pmaxi, maxi)

# Tests
assert top_likes({'Bob': 102, 'Ada': 201, 'Alice': 103,
                  'Tim': 50}) == ('Ada', 201)
assert top_likes({'Alan': 222, 'Ada': 201, 'Eve': 222,
                  'Tim': 50}) == ('Alan', 222)
assert top_likes({'David': 222, 'Ada': 201, 'Alan': 222,
                  'Tim': 50}) == ('Alan', 222)
assert top_likes({'David': 0, 'Ada': 0, 'Alan': 0, 'Tim': 0}) == ('Ada', 0)