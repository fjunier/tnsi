#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 22:25:32 2023

@author: fjunier
"""
def est_trie(tableau):
    """à compléter"""
    for i in range(len(tableau) - 1):
        if tableau[i] > tableau[i + 1]:
            return False
    return True


# tests

assert     est_trie([0, 5, 8, 8, 9])
assert not est_trie([8, 12, 4])
assert     est_trie([-1, 4])
assert     est_trie([5])
assert     est_trie([])
