#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 00:09:40 2023

@author: fjunier
"""


def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime.
    """
    score = dict()
    for personne in votes:
        if personne not in score:
            score[personne] = 0
        for choix in votes[personne]:
            if choix in score:
                score[choix] = score[choix] + 1
            else:
                score[choix] = 1
    return score


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de : J'aime
    """
    score = scores_aimes(votes)
    lis_max = []
    maxi = 0
    for personne in score:
        if score[personne] > maxi:
            maxi = score[personne]
            lis_max = [personne]
        elif score[personne] == maxi:
            lis_max.append(personne)
    return lis_max


# Tests
votes_soiree = {
    "Alice": ["Bob", "Carole", "Dylan"],
    "Bob": ["Carole", "Esma"],
    "Esma": ["Bob", "Alice"],
    "Fabien": ["Dylan"],
    "Carole": [],
    "Dylan": [],
}


assert scores_aimes(votes_soiree) == {
    "Bob": 2,
    "Alice": 1,
    "Esma": 1,
    "Fabien": 0,
    "Carole": 2,
    "Dylan": 2,
}

assert sorted(gagnants(votes_soiree)) == ["Bob", "Carole", "Dylan"]
