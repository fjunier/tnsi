def ou_exclusif(liste_1, liste_2):
    rep = []
    for i in range(len(liste_1)):
        if ((liste_1[i] == 1) and (liste_2[i] == 0)) or ((liste_1[i] == 0) and (liste_2[i] == 1)):
            rep.append(1)
        else:
            rep.append(0)
    return rep

# Tests
liste_a = [1, 0, 1, 0, 1, 1, 0, 1]
liste_b = [0, 1, 1, 1, 0, 1, 0, 0]
liste_c = [1, 1, 0, 1]
liste_d = [0, 0, 1, 1]
assert ou_exclusif(liste_a, liste_b) == [1, 1, 0, 1, 1, 0, 0, 1]
assert ou_exclusif(liste_c, liste_d) == [1, 1, 1, 0]
