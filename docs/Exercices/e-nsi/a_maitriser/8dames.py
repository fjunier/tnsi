def donne_ligne(echiquier, i):
    return echiquier[i]


def donne_colonne(echiquier, j):
    return [echiquier[i][j] for i in range(len(echiquier))]


def donne_diagonale_NE(echiquier, i, j):
    return [echiquier[i - k][j + k] for k in range(min(len(echiquier[0]) - j, i + 1))]


def donne_diagonale_NO(echiquier, i, j):
    return [echiquier[i - k][j - k] for k in range(min(j + 1, i + 1))]


def donne_diagonale_SE(echiquier, i, j):
    return [echiquier[i + k][j + k] for k in range(min(len(echiquier[0]) - j, len(echiquier[0]) - i))]


def donne_diagonale_SO(echiquier, i, j):
    return [echiquier[i + k][j - k] for k in range(min(j + 1, len(echiquier[0]) - i))]


def disposition_valide(echiquier):
    for i in range(len(echiquier)):
        for j in range(len(echiquier[i])):
            nbalig = (sum(donne_colonne(echiquier, j)) + sum(donne_ligne(echiquier, i)) 
            + sum(donne_diagonale_NE(echiquier, i, j))  + sum(donne_diagonale_NO(echiquier, i, j))
            + sum(donne_diagonale_SE(echiquier, i, j)) + sum(donne_diagonale_SO(echiquier, i, j)))
            if nbalig > 6:
                return False
    return True
            


# Tests
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert donne_ligne(valide, 0) == [0, 0, 0, 1, 0, 0, 0, 0]
assert donne_colonne(valide, 2) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_NE(valide, 4, 0) == [0, 0, 1, 0, 0]
assert donne_diagonale_NO(valide, 1, 1) == [0, 0]
assert donne_diagonale_SE(valide, 0, 0) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_SO(valide, 3, 7) == [1, 0, 0, 0, 0]
assert disposition_valide(valide)
assert not disposition_valide(invalide)

