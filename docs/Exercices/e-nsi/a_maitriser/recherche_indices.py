#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 22:53:06 2022

@author: fjunier
"""
def indices(element, entiers):
    pos = []
    for k in range(len(entiers)):
        if entiers[k] == element:
            pos.append(k)
    return pos


# tests

assert indices(3, [3, 2, 1, 3, 2, 1]) == [0, 3]
assert indices(4, [1, 2, 3]) == []
assert indices(1, [1, 1, 1, 1]) == [0, 1, 2, 3]
assert indices(5, [0, 0, 5]) == [2]
