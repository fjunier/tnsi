#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 21:34:31 2023

@author: fjunier
"""
def moyenne(notes_ponderees):
    sv = 0.0
    sc = 0
    for (v, c) in notes_ponderees:
        sc = sc + c
        sv = sv + v * c
    return sv / sc





# tests

def sont_proches(x, y):
    "Renvoie un booléen : les nombres x et y sont-ils proches ?"
    return abs(x - y) < 10**-6

assert sont_proches(moyenne([(15.0, 2)]), 15.0)
assert sont_proches(moyenne([(15.0, 2), (9.0, 1), (12.0, 3)]), 12.5)

