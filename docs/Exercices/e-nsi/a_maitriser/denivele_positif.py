# Code à compléter
def denivele_positif(altitudes):
    dp = 0
    for i in range(0, len(altitudes) - 1):
        d = altitudes[i + 1] - altitudes[i]
        if d > 0:
            dp = dp + d
    return dp


assert denivele_positif([330, 490, 380, 610, 780, 550]) == 560
assert denivele_positif([200, 300, 100]) == 100
