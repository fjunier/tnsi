SYMBOLES = "+-*/()"


def analyse(expression):
    rep = []
    courant = ''
    for c in expression:
        if c in SYMBOLES:
            if len(courant) > 0:
                rep.append(courant)
            courant = ''
            rep.append(c)
        elif c != ' ':
            courant +=  c
    if len(courant) > 0:
        rep.append(courant)
    return rep
    

        


# Tests
assert analyse("35.896 ") == ["35.896"]
assert analyse("3*5   +8") == ["3", "*", "5", "+", "8"]
assert analyse("3.9  * (5+8.6)") == ["3.9", "*", "(", "5", "+", "8.6", ")"]

