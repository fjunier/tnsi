def supprime_premier(chaine, cible):
    chaine_tronque = ""
    present = False
    for c in chaine:
        if c == cible and (not present):
            present = True
        else:
            chaine_tronque = chaine_tronque + c
    return (present, chaine_tronque)


def anagrammes(chaine1, chaine2):
    if len(chaine1) == 1 and len(chaine2) == 1:
        return chaine1 == chaine2
    present, chaine2_tronque = supprime_premier(chaine2, chaine1[0])
    if present:
        return anagrammes(chaine1[1:], chaine2_tronque)
    return False



# Tests
assert supprime_premier('ukulélé', 'u') == (True, 'kulélé')
assert supprime_premier('ukulélé', 'é') == (True, 'ukullé')
assert supprime_premier('ukulélé', 'a') == (False, 'ukulélé')
assert anagrammes('chien', 'niche')
assert anagrammes('énergie noire', 'reine ignorée')
assert not anagrammes('louve', 'poule')

