site_name: "Terminale NSI Lycée du Parc"
site_author: "Frédéric Junier"
site_url: https://fjunier.forge.apps.education.fr/tnsi/
edit_uri: tree/main/docs/
site_description: "Site  de  terminale  NSI  du    Lycée  du  Parc  à Lyon "

#use_directory_urls: true
#voir https://www.mkdocs.org/user-guide/configuration/#docs_dir

# nav:
#   - Progression: index.md
#   - POO: POO/cours_POO.md
#   - Modèle: Modèle/modèle.md 
  

theme:
  name: pyodide-mkdocs-theme
  font: false
  language: fr

  features:
    #- navigation.instant
    - navigation.tabs
    #- navigation.expand
    - navigation.top
    - toc.integrate
    - header.autohide
    - content.code.annotate 
    - content.code.copy
  
  # palette: # Palettes de couleurs jour/nuit
  #   - media: "(prefers-color-scheme: dark)"
  #     scheme: slate
  #     primary: blue
  #     accent: blue
  #     toggle:
  #       icon: material/weather-night
  #       name: Passer au mode jour
  #   - media: "(prefers-color-scheme: light)"
  #     scheme: default
  #     primary: indigo
  #     accent: indigo
  #     toggle:
  #       icon: material/weather-sunny
  #       name: Passer au mode nuit
  #custom_dir: my_theme_customizations/
  logo: assets/sierpingif.gif
  favicon: assets/sierpingif.gif

markdown_extensions:
  - md_in_html #pour écrire du markdown dans du code HTML voir https://mooc-forums.inria.fr/moocnsi/t/mkdocs-astuce-pour-centrer-une-image-en-markdown-pur/2831/2  - meta
  - abbr
  - def_list # Les listes de définition.
  - attr_list # Un peu de CSS et des attributs HTML.
  - admonition # Blocs colorés  !!! info "ma remarque"
  - pymdownx.details #   qui peuvent se plier/déplier.
  - footnotes # Notes[^1] de bas de page.  [^1]: ma note.
  - pymdownx.caret # Passage ^^souligné^^ ou en ^exposant^.
  - pymdownx.mark # Passage ==surligné==.
  - pymdownx.tilde # Passage ~~barré~~ ou en ~indice~.
  - pymdownx.snippets # Inclusion de fichiers externe.
  - pymdownx.highlight: # Coloration syntaxique du code
      auto_title: true
      auto_title_map:
        "Python": "🐍 Script Python"
        "Python Console Session": "🐍 Console Python"
        "Text Only": "📋 Texte"
        "E-mail": "📥 Entrée"
        "Text Output": "📤 Sortie"

  - pymdownx.tasklist: # Cases à cocher  - [ ]  et - [x]
      custom_checkbox: false #   avec cases d'origine
      clickable_checkbox: true #   et cliquables.
  - pymdownx.inlinehilite # pour  `#!python  <python en ligne>`
  - pymdownx.superfences: # Imbrication de blocs.
      preserve_tabs: true
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.keys # Touches du clavier.  ++ctrl+d++
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.emoji: # Émojis  :boom:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  - pymdownx.arithmatex:
      generic: true
      smart_dollar: false
  - toc:
      permalink: ⚓︎
      toc_depth: 3
  #- mkdocs_graphviz #https://gitlab.com/rodrigo.schwencke/mkdocs-graphviz
  
plugins:

    - awesome-pages:
        collapse_single_pages: true
  
    # maj du 26/08/2024 https://mooc-forums.inria.fr/moocnsi/t/nouveau-tuto-creation-de-site/11601/9
    - search
    - tags:
        tags_file: tags.md
    - pyodide_macros:
        # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
        on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.


# En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
# exclu du site final et donc également de l'indexation de la recherche.
# Nota: ne pas mettre de commentaires dans ces lignes !
exclude_docs: |
    **/*_REM.md
    **/*.py
extra:
  social:
    - icon: fontawesome/solid/paper-plane
      link: mailto:admin@frederic-junier.org
      name: Mail

    - icon: fontawesome/brands/gitlab
      link: https://forge.apps.education.fr/fjunier/tnsi
      name: Dépôt git

    - icon: fontawesome/solid/school
      link: https://lyceeduparc.fr/ldp/
      name: Lycée du Parc


  io_url: https://fjunier.forge.apps.education.fr/tnsi/

copyright: Frédéric Junier, contenu sous licence CC BY-NC-SA 4.0

# extra_javascript:
#   - javascripts/config.js
#   #- javascripts/pyodid.js
#   # - xtra/javascripts/interpreter.js
#   #- javascripts/repl.js
#   - https://polyfill.io/v3/polyfill.min.js?features=es6
#   - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
#   #- https://cdn.jsdelivr.net/pyodide/v0.17.0/full/pyodide.js    #chargé dans l'extension du template html overrides/main.html

extra_css:
  - extras/stylesheets/ajustements.css # ajustements
  - stylesheets/extra.css
