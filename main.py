# Debut copie
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin


import os
import hashlib
from math import log10
import random
import re
from urllib.parse import unquote

# Fin copie




def define_env(env:PyodideMacrosPlugin):
    "Hook function"

    # activate trace
    chatter = env.start_chatting("Simple module")  # activate trace
    # voir https://mkdocs-macros-plugin.readthedocs.io/en/latest/troubleshooting/
    env.variables["compteur_exo"] = 0
    env.variables["compteur_exemple"] = 0
    env.variables["compteur_auto"] = 0
    env.variables["compteur_enigme"] = 0
    env.variables["compteur_methode"] = 0
    env.variables["compteur_cours"] = 0
    env.variables["compteur_attention"] = 0
    env.variables["term_counter"] = 0
    env.variables["IDE_counter"] = 0
    env.variables["compteur_remarque"] = 0

    @env.macro
    def exo():  # G Connan
        env.variables["compteur_exo"] += 1
        return f"faq \"Exercice { env.variables['compteur_exo']}\""
    
    @env.macro
    def exemple():  # G Connan
        env.variables["compteur_exemple"] += 1
        return f"example \"Exemple { env.variables['compteur_exemple']}\""
    
    
    @env.macro
    def remarque():  # G Connan
        env.variables["compteur_remarque"] += 1
        return f"note \"Remarque { env.variables['compteur_remarque']}\""
    
    @env.macro
    def methode():  # G Connan
        env.variables["compteur_methode"] += 1
        return f"tip \"Méthode { env.variables['compteur_methode']}\""
    
    @env.macro
    def point_cours():  # G Connan
        env.variables["compteur_cours"] += 1
        return f"tldr \"Point de cours { env.variables['compteur_cours']}\""
    
    @env.macro
    def attention():  # G Connan
        env.variables["compteur_attention"] += 1
        return f"warning \"Attention { env.variables['compteur_attention']}\""
    
    
    @env.macro
    def automatisme():  # G Connan
        env.variables["compteur_auto"] += 1
        return f"tip \"Automatisme { env.variables['compteur_auto']}\""

    @env.macro
    def enigme():
        env.variables["compteur_enigme"] += 1
        return f"question \"Énigme { env.variables['compteur_enigme']}\""

   
     
